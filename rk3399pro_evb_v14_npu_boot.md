# EVB v14, SDK v11 (origin)

```
DDR Version V1.02 20190312
LPDDR3
933MHz
BW=32 Col=10 Bk=8 CS0 Row=15 CS1 Row=15 CS=2 Die BW=32 Size=2048MB
out
Returning to boot ROM...
Boot1 Release Time: May 21 2019 20:12:00, version: 1.03
ChipType = 0x14, 168
SecureInit ret = 0, SecureMode = 0
atags_set_bootdev: ret:(0)
NPU UsbBoot...
UsbHook ...10508
powerOn 12732
USBParse: cmd:0x18, sub_code:0xaa, len:56                                       
MAGIC: 4d505352                                                                 
0 Type:2, Addr:0x8000000                                                        
UbootAddr:0x8000000                                                             
1 Type:1, Addr:0x8100000                                                        
TrustAddr:0x8100000                                                             
2 Type:4, Addr:0x4000000                                                        
BootAddr:0x4000000                                                              
atags_set_boot_addr: ret:(0)                                                    
USBParse:ret:0                                                                  
USBParse: cmd:0x19, sub_code:0xaa, len:0                                        
LoadTrust Addr:0x40800                                                          
No find bl30.bin                                                                
No find bl32.bin                                                                
Load uboot, ReadLba = 40000                                                     
Load OK, addr=0x600000, size=0x41eb0                                            
USB Boot OK!                                                                    
RunBL31 0x10000                                                                 
INFO:    Preloader serial: 2                                                   
NOTICE:  BL31: v1.3(debug):301942b                                              
NOTICE:  BL31: Built : 17:24:17, May 21 2019                                    
NOTICE:  BL31: Rockchip release version: v1.0                                   
INFO:    GICv3 with legacy support detected. ARM GICV3 driver initialized in EL3
INFO:    boot from ram                                                          
INFO:    Using opteed sec cpu_context!                                          
INFO:    boot cpu mask: 0                                                       
INFO:    plat_rockchip_pmu_init(1596): pd status 8002                           
INFO:    BL31: Initializing runtime services                                    
WARNING: No OPTEE provided by BL2 boot loader, Booting device without OPTEE iniK
ERROR:   Error initializing runtime service opteed_fast                         
INFO:    BL31: Preparing for EL3 exit to normal world                           
INFO:    Entry point address = 0x600000                                         
INFO:    SPSR = 0x3c9                          
```

# EVB v14, SDK v14

```
hello
```
