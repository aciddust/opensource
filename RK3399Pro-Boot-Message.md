```bash
DDR Version 1.22 20190506
In
Channel 0: LPDDR3, 800MHz
CS = 0
MR0=0x58
MR1=0x58
MR2=0x58
MR3=0x58
MR4=0x2
MR5=0x1
MR6=0x5
MR7=0x0
MR8=0x1F
MR9=0x1F
MR10=0x1F
MR11=0x1F
MR12=0x1F
MR13=0x1F
MR14=0x1F
MR15=0x1F
MR16=0x1F
CS = 1
MR0=0x58
MR1=0x58
MR2=0x58
MR3=0x58
MR4=0x2
MR5=0x1
MR6=0x5
MR7=0x0
MR8=0x1F
MR9=0x1F
MR10=0x1F
MR11=0x1F
MR12=0x1F
MR13=0x1F
MR14=0x1F
MR15=0x1F
MR16=0x1F
Bus Width=32 Col=10 Bank=8 Row=15/15 CS=2 Die Bus-Width=32 Size=2048MB
Channel 1: LPDDR3, 800MHz
CS = 0
MR0=0x58
MR1=0x58
MR2=0x58
MR3=0x58
MR4=0x2
MR5=0x1
MR6=0x5
MR7=0x0
MR8=0x1F
MR9=0x1F
MR10=0x1F
MR11=0x1F
MR12=0x1F
MR13=0x1F
MR14=0x1F
MR15=0x1F
MR16=0x1F
CS = 1
MR0=0x58
MR1=0x58
MR2=0x58
MR3=0x58
MR4=0x2
MR5=0x1
MR6=0x5
MR7=0x0
MR8=0x1F
MR9=0x1F
MR10=0x1F
MR11=0x1F
MR12=0x1F
MR13=0x1F
MR14=0x1F
MR15=0x1F
MR16=0x1F
Bus Width=32 Col=10 Bank=8 Row=15/15 CS=2 Die Bus-Width=32 Size=2048MB
256B stride
ch 0 ddrconfig = 0x101, ddrsize = 0x2020
ch 1 ddrconfig = 0x101, ddrsize = 0x2020
pmugrf_os_reg[2] = 0x3AA0DAA0, stride = 0xD
OUT
Boot1: 2018-08-06, version: 1.15
CPUId = 0x0
ChipType = 0x10, 216
SdmmcInit=2 0
BootCapSize=100000
UserCapSize=14910MB
FwPartOffset=2000 , 100000
mmc0:cmd8,20
mmc0:cmd5,20
mmc0:cmd55,20
mmc0:cmd1,20
mmc0:cmd8,20
mmc0:cmd5,20
mmc0:cmd55,20
mmc0:cmd1,20
mmc0:cmd8,20
mmc0:cmd5,20
mmc0:cmd55,20
mmc0:cmd1,20
SdmmcInit=0 1
StorageInit ok = 67800
SecureMode = 0
SecureInit read PBA: 0x4
SecureInit read PBA: 0x404
SecureInit read PBA: 0x804
SecureInit read PBA: 0xc04
SecureInit read PBA: 0x1004
SecureInit read PBA: 0x1404
SecureInit read PBA: 0x1804
SecureInit read PBA: 0x1c04
SecureInit ret = 0, SecureMode = 0
GPT part:  0, name:            uboot, start:0x4000, size:0x2000
GPT part:  1, name:            trust, start:0x6000, size:0x2000
GPT part:  2, name:             misc, start:0x8000, size:0x2000
GPT part:  3, name:             boot, start:0xa000, size:0x10000
GPT part:  4, name:         recovery, start:0x1a000, size:0x30000
GPT part:  5, name:           backup, start:0x4a000, size:0x10000
GPT part:  6, name:              oem, start:0x5a000, size:0x20000
GPT part:  7, name:           rootfs, start:0x7a000, size:0x700000
GPT part:  8, name:         userdata, start:0x77a000, size:0x15a4fdf
find partition:uboot OK. first_lba:0x4000.
find partition:trust OK. first_lba:0x6000.
LoadTrust Addr:0x6000
No find bl30.bin
Load uboot, ReadLba = 4000
Load OK, addr=0x200000, size=0xc0694
RunBL31 0x10000
NOTICE:  BL31: v1.3(debug):51f2096
NOTICE:  BL31: Built : 16:24:31, May  6 2019
NOTICE:  BL31: Rockchip release version: v1.1
INFO:    GICv3 with legacy support detected. ARM GICV3 driver initialized in EL3
INFO:    Using opteed sec cpu_context!
INFO:    boot cpu mask: 0
INFO:    plat_rockchip_pmu_init(1181): pd status 3e
INFO:    BL31: Initializing runtime services
INFO:    BL31: Initializing BL32
INF [0x0] TEE-CORE:init_primary_helper:337: Initializing (1.1.0-195-g8f090d20 #6 Fri Dec  7 06:11:20 UTC 2018 aarch64)

INF [0x0] TEE-CORE:init_primary_helper:338: Release version: 1.2

INF [0x0] TEE-CORE:init_teecore:83: teecore inits done
INFO:    BL31: Preparing for EL3 exit to normal world
INFO:    Entry point address = 0x200000
INFO:    SPSR = 0x3c9


U-Boot 2017.09-g6999767-dirty (Aug 26 2019 - 17:15:17 +0900)

Model: Rockchip RK3399 Evaluation Board
PreSerial: 2
DRAM:  3.8 GiB
Sysmem: init
Relocation Offset is: f7cf5000
Using default environment

dwmmc@fe320000: 1, sdhci@fe330000: 0
Card did not respond to voltage select!
mmc_init: -95, time 9
switch to partitions #0, OK
mmc0(part 0) is current device
Bootdev: mmc 0
PartType: EFI
boot mode: None
Load FDT from boot part
DTB: rk-kernel.dtb
Android header version 0
I2c speed: 400000Hz
PMIC:  RK8090 (on=0x40, off=0x00)
vdd_center 900000 uV
vdd_cpu_l 900000 uV
[SDM] rockchip_show_logo done
CLK: (uboot. arml: enter 816000 KHz, init 816000 KHz, kernel 0N/A)
CLK: (uboot. armb: enter 24000 KHz, init 24000 KHz, kernel 0N/A)
  aplll 816000 KHz
  apllb 24000 KHz
  dpll 800000 KHz
  cpll 24000 KHz
  gpll 800000 KHz
  npll 600000 KHz
  vpll 24000 KHz
  aclk_perihp 133333 KHz
  hclk_perihp 66666 KHz
  pclk_perihp 33333 KHz
  aclk_perilp0 266666 KHz
  hclk_perilp0 88888 KHz
  pclk_perilp0 44444 KHz
  hclk_perilp1 100000 KHz
  pclk_perilp1 50000 KHz
[SDM] soc_clk_dump done
Net:   eth0: ethernet@fe300000
Hit key to stop autoboot('CTRL+C'):  0 
ANDROID: reboot reason: "(none)"
Not AVB images, AVB skip
Fdt Ramdisk skip relocation
FDT load addr 0x10f00000 size 135 KiB
Booting IMAGE kernel at 0x00280000 with fdt at 0x8300000...


## Booting Android Image at 0x0027f800 ...
Kernel load addr 0x00280000 size 16421 KiB
## Flattened Device Tree blob at 08300000
   Booting using the fdt blob at 0x8300000
   XIP Kernel Image ... OK
  'reserved-memory' region@110000: addr=110000 size=f0000
   Using Device Tree in place at 0000000008300000, end 000000000831befb
Adding bank: 0x00200000 - 0x08400000 (size: 0x08200000)
Adding bank: 0x0a200000 - 0xf8000000 (size: 0xede00000)
Total: 579.26 ms

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct
[    0.000000] Linux version 4.4.167 (root@tk-Z97X-UD3H) (gcc version 6.3.1 20170404 (Linaro GCC 6.3-2017.05) ) #1 SMP Thu Aug 22 11:58:03 KST 2019
[    0.000000] Boot CPU: AArch64 Processor [410fd034]
[    0.000000] earlycon: Early serial console at MMIO32 0xff1a0000 (options '')
[    0.000000] bootconsole [uart0] enabled
[    0.000000] Reserved memory: failed to reserve memory for node 'drm-logo@00000000': base 0x0000000000000000, size 0 MiB
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.0 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: Trusted OS migration not required
[    0.000000] PERCPU: Embedded 21 pages/cpu @ffffffc0f7ecf000 s45800 r8192 d32024 u86016
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: enabling workaround for ARM erratum 845719
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 991752
[    0.000000] Kernel command line: storagemedia=emmc androidboot.mode=normal androidboot.verifiedbootstate=orange androidboot.slot_suffix= androidboot.serialno=4c564160ea3fdc6d  rw rootwait earlycon=uart82m
[    0.000000] PID hash table entries: 4096 (order: 3, 32768 bytes)
[    0.000000] Dentry cache hash table entries: 524288 (order: 10, 4194304 bytes)
[    0.000000] Inode-cache hash table entries: 262144 (order: 9, 2097152 bytes)
[    0.000000] software IO TLB [mem 0xf7e86000-0xf7ec6000] (0MB) mapped at [ffffffc0f7e86000-ffffffc0f7ec5fff]
[    0.000000] Memory: 3938748K/4030464K available (9854K kernel code, 1508K rwdata, 3972K rodata, 1024K init, 1935K bss, 91716K reserved, 0K cma-reserved)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     modules : 0xffffff8000000000 - 0xffffff8008000000   (   128 MB)
[    0.000000]     vmalloc : 0xffffff8008000000 - 0xffffffbdbfff0000   (   246 GB)
[    0.000000]       .init : 0xffffff8008e10000 - 0xffffff8008f10000   (  1024 KB)
[    0.000000]       .text : 0xffffff8008080000 - 0xffffff8008a20000   (  9856 KB)
[    0.000000]     .rodata : 0xffffff8008a20000 - 0xffffff8008e10000   (  4032 KB)
[    0.000000]       .data : 0xffffff8008f10000 - 0xffffff8009089008   (  1509 KB)
[    0.000000]     vmemmap : 0xffffffbdc0000000 - 0xffffffbfc0000000   (     8 GB maximum)
[    0.000000]               0xffffffbdc0008000 - 0xffffffbdc3e00000   (    61 MB actual)
[    0.000000]     fixed   : 0xffffffbffe7fb000 - 0xffffffbffec00000   (  4116 KB)
[    0.000000]     PCI I/O : 0xffffffbffee00000 - 0xffffffbfffe00000   (    16 MB)
[    0.000000]     memory  : 0xffffffc000200000 - 0xffffffc0f8000000   (  3966 MB)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=6, Nodes=1
[    0.000000] Hierarchical RCU implementation.
[    0.000000]  Build-time adjustment of leaf fanout to 64.
[    0.000000]  RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=6.
[    0.000000] RCU: Adjusting geometry for rcu_fanout_leaf=64, nr_cpu_ids=6
[    0.000000] NR_IRQS:64 nr_irqs:64 0
[    0.000000] GIC: Using split EOI/Deactivate mode
[    0.000000] ITS: /interrupt-controller@fee00000/interrupt-controller@fee20000
[    0.000000] ITS: allocated 65536 Devices @a280000 (psz 64K, shr 0)
[    0.000000] ITS: using cache flushing for cmd queue
[    0.000000] GIC: using LPI property table @0x000000000a210000
[    0.000000] ITS: Allocated 1792 chunks for LPIs
[    0.000000] CPU0: found redistributor 0 region 0:0x00000000fef00000
[    0.000000] CPU0: using LPI pending table @0x000000000a220000
[    0.000000] GIC: using cache flushing for LPI property table
[    0.000000] GIC: PPI partition interrupt-partition-0[0] { /cpus/cpu@0[0] /cpus/cpu@1[1] /cpus/cpu@2[2] /cpus/cpu@3[3] }
[    0.000000] GIC: PPI partition interrupt-partition-1[1] { /cpus/cpu@100[4] /cpus/cpu@101[5] }
[    0.000000] rockchip_clk_register_frac_branch: could not find dclk_vop0_frac as parent of dclk_vop0, rate changes may not work
[    0.000000] rockchip_clk_register_frac_branch: could not find dclk_vop1_frac as parent of dclk_vop1, rate changes may not work
[    0.000000] rockchip_cpuclk_pre_rate_change: limiting alt-divider 33 to 31
[    0.000000] Architected cp15 timer(s) running at 24.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x588fe9dc0, max_idle_ns: 440795202592 ns
[    0.000005] sched_clock: 56 bits at 24MHz, resolution 41ns, wraps every 4398046511097ns
[    0.002265] Console: colour dummy device 80x25
[    0.002707] Calibrating delay loop (skipped), value calculated using timer frequency.. 48.00 BogoMIPS (lpj=24000)
[    0.003697] pid_max: default: 32768 minimum: 301
[    0.004277] Mount-cache hash table entries: 8192 (order: 4, 65536 bytes)
[    0.004930] Mountpoint-cache hash table entries: 8192 (order: 4, 65536 bytes)
[    0.006388] Initializing cgroup subsys devices
[    0.006823] Initializing cgroup subsys freezer
[    0.007284] ftrace: allocating 37520 entries in 147 pages
[    0.104740] sched-energy: Sched-energy-costs installed from DT
[    0.105309] CPU0: update cpu_capacity 401
[    0.105758] ASID allocator initialised with 32768 entries
[    0.109371] PCI/MSI: /interrupt-controller@fee00000/interrupt-controller@fee20000 domain created
[    0.110768] Platform MSI: /interrupt-controller@fee00000/interrupt-controller@fee20000 domain created
[    0.113030] Detected VIPT I-cache on CPU1
[    0.113063] CPU1: found redistributor 1 region 0:0x00000000fef20000
[    0.113092] CPU1: using LPI pending table @0x00000000f2ca0000
[    0.113135] CPU1: update cpu_capacity 401
[    0.113139] CPU1: Booted secondary processor [410fd034]
[    0.113686] Detected VIPT I-cache on CPU2
[    0.113707] CPU2: found redistributor 2 region 0:0x00000000fef40000
[    0.113735] CPU2: using LPI pending table @0x00000000f2cc0000
[    0.113762] CPU2: update cpu_capacity 401
[    0.113765] CPU2: Booted secondary processor [410fd034]
[    0.114292] Detected VIPT I-cache on CPU3
[    0.114312] CPU3: found redistributor 3 region 0:0x00000000fef60000
[    0.114339] CPU3: using LPI pending table @0x00000000f2d10000
[    0.114365] CPU3: update cpu_capacity 401
[    0.114369] CPU3: Booted secondary processor [410fd034]
[    0.114923] Detected PIPT I-cache on CPU4
[    0.114949] CPU4: found redistributor 100 region 0:0x00000000fef80000
[    0.114988] CPU4: using LPI pending table @0x00000000f2d50000
[    0.115027] CPU4: update cpu_capacity 1024
[    0.115030] CPU4: Booted secondary processor [410fd082]
[    0.115583] Detected PIPT I-cache on CPU5
[    0.115601] CPU5: found redistributor 101 region 0:0x00000000fefa0000
[    0.115638] CPU5: using LPI pending table @0x00000000f2d80000
[    0.115663] CPU5: update cpu_capacity 1024
[    0.115666] CPU5: Booted secondary processor [410fd082]
[    0.115766] Brought up 6 CPUs
[    0.128128] SMP: Total of 6 processors activated.
[    0.128579] CPU features: detected feature: GIC system register CPU interface
[    0.129268] CPU features: detected feature: 32-bit EL0 Support
[    0.129828] CPU: All CPU(s) started at EL2
[    0.130259] alternatives: patching kernel code
[    0.132076] devtmpfs: initialized
[    0.149408] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275000 ns
[    0.150346] futex hash table entries: 2048 (order: 5, 131072 bytes)
[    0.151397] pinctrl core: initialized pinctrl subsystem
[    0.152621] NET: Registered protocol family 16
[    0.193075] console [ttyFIQ0] enabled
[    0.193075] console [ttyFIQ0] enabled
[    0.193761] bootconsole [uart0] disabled
[    0.193761] bootconsole [uart0] disabled
[    0.194654] Registered fiq debugger ttyFIQ0
[    0.209491] iommu: Adding device ff650000.vpu_service to group 0
[    0.210097] iommu: Adding device ff660000.rkvdec to group 1
[    0.210708] iommu: Adding device ff8f0000.vop to group 2
[    0.211264] iommu: Adding device ff900000.vop to group 3
[    0.211829] iommu: Adding device ff910000.rkisp1 to group 4
[    0.212417] iommu: Adding device ff920000.rkisp1 to group 5
[    0.213389] rk_iommu ff650800.iommu: can't get sclk
[    0.214106] rk_iommu ff660480.iommu: can't get sclk
[    0.214755] rk_iommu ff8f3f00.iommu: can't get sclk
[    0.215370] rk_iommu ff903f00.iommu: can't get sclk
[    0.215969] rk_iommu ff914000.iommu: can't get sclk
[    0.216579] rk_iommu ff924000.iommu: can't get sclk
[    0.217372] SCSI subsystem initialized
[    0.217888] usbcore: registered new interface driver usbfs
[    0.218422] usbcore: registered new interface driver hub
[    0.218971] usbcore: registered new device driver usb
[    0.219472] media: Linux media interface: v0.10
[    0.219914] Linux video capture interface: v2.00
[    0.220404] pps_core: LinuxPPS API ver. 1 registered
[    0.220850] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.221680] PTP clock support registered
[    0.223153] Advanced Linux Sound Architecture Driver Initialized.
[    0.224174] Bluetooth: Core ver 2.21
[    0.224514] NET: Registered protocol family 31
[    0.224923] Bluetooth: HCI device and connection manager initialized
[    0.225490] Bluetooth: HCI socket layer initialized
[    0.225935] Bluetooth: L2CAP socket layer initialized
[    0.226398] Bluetooth: SCO socket layer initialized
[    0.227442] rockchip-cpuinfo cpuinfo: Serial         : 4c564160ea3fdc6d
[    0.228524] clocksource: Switched to clocksource arch_sys_counter
[    0.277081] thermal thermal_zone1: power_allocator: sustainable_power will be estimated
[    0.277997] NET: Registered protocol family 2
[    0.278837] TCP established hash table entries: 32768 (order: 6, 262144 bytes)
[    0.279750] TCP bind hash table entries: 32768 (order: 8, 1048576 bytes)
[    0.281278] TCP: Hash tables configured (established 32768 bind 32768)
[    0.281971] UDP hash table entries: 2048 (order: 5, 196608 bytes)
[    0.282680] UDP-Lite hash table entries: 2048 (order: 5, 196608 bytes)
[    0.283595] NET: Registered protocol family 1
[    0.284287] RPC: Registered named UNIX socket transport module.
[    0.284831] RPC: Registered udp transport module.
[    0.285254] RPC: Registered tcp transport module.
[    0.285684] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.287307] hw perfevents: enabled with armv8_cortex_a53 PMU driver, 7 counters available
[    0.288141] hw perfevents: enabled with armv8_cortex_a72 PMU driver, 7 counters available
[    0.291927] Initialise system trusted keyring
[    0.300193] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.301491] NFS: Registering the id_resolver key type
[    0.301981] Key type id_resolver registered
[    0.302359] Key type id_legacy registered
[    0.302789] fuse init (API version 7.23)
[    0.303507] SGI XFS with security attributes, no debug enabled
[    0.308898] NET: Registered protocol family 38
[    0.309319] Key type asymmetric registered
[    0.309704] Asymmetric key parser 'x509' registered
[    0.310292] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 246)
[    0.310967] io scheduler noop registered
[    0.311326] io scheduler deadline registered
[    0.311831] io scheduler cfq registered (default)
[    0.313279] rockchip-usb2phy ff770000.syscon:usb2-phy@e460: failed to create phy
[    0.315439] rockchip-mipi-dphy-rx ff770000.syscon:mipi-dphy-rx0: invalid resource
[    0.316558] rockchip-mipi-dphy-rx ff968000.mipi-dphy-tx1rx1: bad remote port parent
[    0.317277] rockchip-mipi-dphy-rx: probe of ff968000.mipi-dphy-tx1rx1 failed with error -22
[    0.321994] rockchip-pcie f8000000.pcie: no vpcie3v3 regulator found
[    0.322594] rockchip-pcie f8000000.pcie: no vpcie1v8 regulator found
[    0.323180] rockchip-pcie f8000000.pcie: no vpcie0v9 regulator found
[    0.323751] rockchip-pcie f8000000.pcie: missing "memory-region" property
[    0.324356] PCI host bridge /pcie@f8000000 ranges:
[    0.324801]   MEM 0xfa000000..0xfbdfffff -> 0xfa000000
[    0.325261]    IO 0xfbe00000..0xfbefffff -> 0xfbe00000
[    0.867587] rockchip-pcie f8000000.pcie: PCIe link training gen1 timeout!
[    0.868207] rockchip-pcie f8000000.pcie: deferred probe failed
[    0.868851] rockchip-pcie: probe of f8000000.pcie failed with error -110
[    0.870403] backlight supply power not found, using dummy regulator
[    0.871739] rk-vcodec ff650000.vpu_service: no regulator for vcodec
[    0.872723] rk-vcodec ff650000.vpu_service: probe device
[    0.873469] rk-vcodec ff650000.vpu_service: drm allocator with mmu enabled
[    0.874861] rk-vcodec ff650000.vpu_service: could not find power_model node
[    0.875482] rk-vcodec ff650000.vpu_service: init success
[    0.876388] rk-vcodec ff660000.rkvdec: no regulator for vcodec
[    0.877425] rk-vcodec ff660000.rkvdec: probe device
[    0.878095] rk-vcodec ff660000.rkvdec: drm allocator with mmu enabled
[    0.879075] rk-vcodec ff660000.rkvdec: could not find power_model node
[    0.879668] rk-vcodec ff660000.rkvdec: init success
[    0.881567] dma-pl330 ff6d0000.dma-controller: Loaded driver for PL330 DMAC-241330
[    0.882239] dma-pl330 ff6d0000.dma-controller:       DBUFF-32x8bytes Num_Chans-6 Num_Peri-12 Num_Events-12
[    0.884495] dma-pl330 ff6e0000.dma-controller: Loaded driver for PL330 DMAC-241330
[    0.885179] dma-pl330 ff6e0000.dma-controller:       DBUFF-128x8bytes Num_Chans-8 Num_Peri-20 Num_Events-16
[    0.887007] rockchip-system-monitor rockchip-system-monitor: system monitor probe
[    0.888345] Serial: 8250/16550 driver, 5 ports, IRQ sharing disabled
[    0.889532] ff180000.serial: ttyS0 at MMIO 0xff180000 (irq = 36, base_baud = 1500000) is a 16550A
[    0.891386] [drm] Initialized drm 1.1.0 20060810
[    0.895803] [drm] Rockchip DRM driver version: v1.0.1
[    0.897769] panel supply power not found, using dummy regulator
[    0.899155] mali ff9a0000.gpu: Failed to get regulator
[    0.899628] mali ff9a0000.gpu: Power control initialization failed
[    0.900543] Unable to detect cache hierarchy for CPU 0
[    0.901783] brd: module loaded
[    0.907203] loop: module loaded
[    0.908051] zram: Added device: zram0
[    0.908699] lkdtm: No crash points registered, enable through debugfs
[    0.910183] rockchip-spi ff1d0000.spi: no high_speed pinctrl state
[    0.912682] rk_gmac-dwmac fe300000.ethernet: clock input or output? (input).
[    0.913308] rk_gmac-dwmac fe300000.ethernet: TX delay(0x28).
[    0.913827] rk_gmac-dwmac fe300000.ethernet: RX delay(0x11).
[    0.914348] rk_gmac-dwmac fe300000.ethernet: integrated PHY? (no).
[    0.915089] rk_gmac-dwmac fe300000.ethernet: cannot get clock clk_mac_speed
[    0.915719] rk_gmac-dwmac fe300000.ethernet: clock input from PHY
[    0.921265] rk_gmac-dwmac fe300000.ethernet: init for RGMII
[    0.921886] stmmac - user ID: 0x10, Synopsys ID: 0x35
[    0.922332]  Ring mode enabled
[    0.922615]  DMA HW capability register supported
[    0.923014]  Normal descriptors
[    0.923324]  RX Checksum Offload Engine supported (type 2)
[    0.923822]  TX Checksum insertion supported
[    0.924198]  Wake-Up On Lan supported
[    0.924566]  Enable RX Mitigation via HW Watchdog Timer
[    0.989784] libphy: stmmac: probed
[    0.990097] eth%d: PHY ID 001cc915 at 0 IRQ POLL (stmmac-0:00) active
[    0.990678] eth%d: PHY ID 001cc915 at 1 IRQ POLL (stmmac-0:01)
[    0.992504] usbcore: registered new interface driver rndis_wlan
[    0.993236] Rockchip WiFi SYS interface (V1.00) ... 
[    0.993767] usbcore: registered new interface driver rtl8150
[    0.994307] usbcore: registered new interface driver r8152
[    0.994856] usbcore: registered new interface driver asix
[    0.995372] usbcore: registered new interface driver ax88179_178a
[    0.995955] usbcore: registered new interface driver cdc_ether
[    0.996505] usbcore: registered new interface driver rndis_host
[    0.997105] usbcore: registered new interface driver cdc_ncm
[    0.997653] usbcore: registered new interface driver cdc_mbim
[    0.999503] rockchip-dwc3 usb0: failed to get drvdata dwc3
[    1.000995] rockchip-dwc3 usb1: failed to get drvdata dwc3
[    1.002468] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    1.003068] ehci-pci: EHCI PCI platform driver
[    1.003524] ehci-platform: EHCI generic platform driver
[    1.004672] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    1.005234] ohci-platform: OHCI generic platform driver
[    1.006656] usbcore: registered new interface driver cdc_acm
[    1.007161] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    1.007911] usbcore: registered new interface driver cdc_wdm
[    1.008445] usbcore: registered new interface driver uas
[    1.008976] usbcore: registered new interface driver usb-storage
[    1.009580] usbcore: registered new interface driver usbserial
[    1.010119] usbcore: registered new interface driver usbserial_generic
[    1.010729] usbserial: USB Serial support registered for generic
[    1.011295] usbcore: registered new interface driver cp210x
[    1.011823] usbserial: USB Serial support registered for cp210x
[    1.012377] usbcore: registered new interface driver ftdi_sio
[    1.012927] usbserial: USB Serial support registered for FTDI USB Serial Device
[    1.013609] usbcore: registered new interface driver keyspan
[    1.014133] usbserial: USB Serial support registered for Keyspan - (without firmware)
[    1.014854] usbserial: USB Serial support registered for Keyspan 1 port adapter
[    1.015528] usbserial: USB Serial support registered for Keyspan 2 port adapter
[    1.016191] usbserial: USB Serial support registered for Keyspan 4 port adapter
[    1.016871] usbcore: registered new interface driver option
[    1.017386] usbserial: USB Serial support registered for GSM modem (1-port)
[    1.018044] usbcore: registered new interface driver oti6858
[    1.018579] usbserial: USB Serial support registered for oti6858
[    1.019133] usbcore: registered new interface driver pl2303
[    1.019656] usbserial: USB Serial support registered for pl2303
[    1.020209] usbcore: registered new interface driver qcserial
[    1.020745] usbserial: USB Serial support registered for Qualcomm USB modem
[    1.021400] usbcore: registered new interface driver sierra
[    1.021924] usbserial: USB Serial support registered for Sierra USB modem
[    1.023533] usbcore: registered new interface driver usbtouchscreen
[    1.024750] i2c /dev entries driver
[    1.026284] rk808 0-0020: Pmic Chip id: 0x8090
[    1.027029] rk808 0-0020: source: on=0x40, off=0x00
[    1.028267] rk808 0-0020: support dcdc3 fb mode:-22, -239060992
[    1.029070] rk808 0-0020: support pmic reset mode:0,0
[    1.029758] rk808 0-0020: no pinctrl handle
[    1.032099] rk808-regulator rk808-regulator: there is no dvs0 gpio
[    1.032724] rk808-regulator rk808-regulator: there is no dvs1 gpio
[    1.033379] DCDC_REG1: supplied by vcc5v0_sys
[    1.035258] DCDC_REG2: supplied by vcc5v0_sys
[    1.037242] DCDC_REG3: supplied by vcc5v0_sys
[    1.038937] DCDC_REG4: supplied by vcc5v0_sys
[    1.040623] DCDC_REG5: supplied by vcc5v0_sys
[    1.042121] LDO_REG1: supplied by vcc_buck5
[    1.044265] LDO_REG2: supplied by vcc_buck5
[    1.046424] LDO_REG3: supplied by vcc_buck5
[    1.048600] LDO_REG4: supplied by vcc_buck5
[    1.050766] LDO_REG5: supplied by vcc_buck5
[    1.052905] LDO_REG6: supplied by vcc_buck5
[    1.055064] LDO_REG7: supplied by vcc3v3_sys
[    1.057218] LDO_REG8: supplied by vcc3v3_sys
[    1.059535] LDO_REG9: supplied by vcc3v3_sys
[    1.061733] SWITCH_REG1: supplied by vcc5v0_sys
[    1.062562] SWITCH_REG2: supplied by vcc3v3_sys
[    1.064138] rk817-battery rk817-battery: Failed to find matching dt id
[    1.064890] rk817-charger rk817-charger: Failed to find matching dt id
[    1.067626] input: rk8xx_pwrkey as /devices/platform/ff3c0000.i2c/i2c-0/0-0020/input/input0
[    1.072090] rk808-rtc rk808-rtc: rtc core: registered rk808-rtc as rtc0
[    1.074589] fan53555-regulator 0-001c: FAN53555 Option[12] Rev[15] Detected!
[    1.075274] fan53555-reg: supplied by vcc5v0_sys
[    1.077963] fan53555-regulator 0-0010: FAN53555 Option[0] Rev[15] Detected!
[    1.078652] fan53555-reg: supplied by vcc5v0_sys
[    1.080726] rk3x-i2c ff3c0000.i2c: Initialized RK3xxx I2C bus at ffffff80092cc000
[    1.082404] rk3x-i2c ff110000.i2c: Initialized RK3xxx I2C bus at ffffff80092ce000
[    1.084160] input: gsl3673 as /devices/platform/ff3d0000.i2c/i2c-4/4-0040/input/input1
[    1.133284] rk3x-i2c ff3d0000.i2c: Initialized RK3xxx I2C bus at ffffff80092d6000
[    1.135671] fusb302 8-0022: Can't get property of role, set role to default DRP
[    1.145673] fusb302 8-0022: port 0 probe success with role ROLE_MODE_DRP, try_role ROLE_MODE_NONE
[    1.146694] input: Typec_Headphone as /devices/platform/ff3e0000.i2c/i2c-8/8-0022/input/input2
[    1.147730] rk3x-i2c ff3e0000.i2c: Initialized RK3xxx I2C bus at ffffff80092de000
[    1.149387] vm149c 1-000c: probing...
[    1.149739] vm149c 1-000c: driver version: 00.01.00
[    1.150179] vm149c 1-000c: could not get module rockchip,vcm-start-current from dts!
[    1.150879] vm149c 1-000c: could not get module rockchip,vcm-rated-current from dts!
[    1.151579] vm149c 1-000c: could not get module rockchip,vcm-step-mode from dts!
[    1.152275] vm149c 1-000c: probing successful
[    1.153532] IR NEC protocol handler initialized
[    1.155078] rkisp1 ff910000.rkisp1: rkisp1 driver version: v00.01.04
[    1.155677] rkisp1 ff910000.rkisp1: Missing rockchip,grf property
[    1.158723] rkisp1 ff920000.rkisp1: rkisp1 driver version: v00.01.04
[    1.159306] rkisp1 ff920000.rkisp1: Missing rockchip,grf property
[    1.161953] usbcore: registered new interface driver uvcvideo
[    1.162468] USB Video Class driver (1.1.1)
[    1.166633] Boot mode: coldboot
[    1.167943] rk_tsadcv2_temp_to_code: Invalid conversion table: code=1023, temperature=2147483647
[    1.168917] rockchip-thermal ff260000.tsadc: tsadc is probed successfully!
[    1.170093] dw_wdt ff848000.watchdog: Should better to setup a 'resets' property in dt, that must been named with reset
[    1.171279] Bluetooth: Virtual HCI driver ver 1.5
[    1.171829] Bluetooth: HCI UART driver ver 2.3
[    1.172231] Bluetooth: HCI UART protocol H4 registered
[    1.172702] Bluetooth: HCI UART protocol LL registered
[    1.173160] Bluetooth: HCI UART protocol ATH3K registered
[    1.173705] usbcore: registered new interface driver bfusb
[    1.174241] usbcore: registered new interface driver btusb
[    1.175213] cpu cpu0: leakage=0
[    1.188253] cpu cpu0: temp=43888, pvtm=143378 (143246 + 132)
[    1.189715] cpu cpu0: pvtm-volt-sel=0
[    1.190396] cpu cpu4: leakage=0
[    1.203214] cpu cpu4: temp=43888, pvtm=154523 (154453 + 70)
[    1.204548] cpu cpu4: pvtm-volt-sel=1
[    1.206936] cpu cpu0: avs=0
[    1.207346] cpu cpu0: l=0 h=2147483647 hyst=5000 l_limit=0 h_limit=0
[    1.209565] cpu cpu0: failed to find power_model node
[    1.211437] cpu cpu4: avs=0
[    1.211857] cpu cpu4: l=0 h=2147483647 hyst=5000 l_limit=0 h_limit=0
[    1.213954] cpu cpu4: failed to find power_model node
[    1.215586] sdhci: Secure Digital Host Controller Interface driver
[    1.216135] sdhci: Copyright(c) Pierre Ossman
[    1.216547] Synopsys Designware Multimedia Card Interface Driver
[    1.218438] dwmmc_rockchip fe310000.dwmmc: IDMAC supports 32-bit address mode.
[    1.219131] dwmmc_rockchip fe310000.dwmmc: Using internal DMA controller.
[    1.219761] dwmmc_rockchip fe310000.dwmmc: Version ID is 270a
[    1.220328] dwmmc_rockchip fe310000.dwmmc: DW MMC controller at irq 25,32 bit host data width,256 deep fifo
[    1.221230] dwmmc_rockchip fe310000.dwmmc: 'clock-freq-min-max' property was deprecated.
[    1.222062] dwmmc_rockchip fe310000.dwmmc: No vmmc regulator found
[    1.222627] dwmmc_rockchip fe310000.dwmmc: No vqmmc regulator found
[    1.224048] rockchip-pinctrl pinctrl: pin gpio4-8 already requested by fiq-debugger; cannot claim for fe320000.dwmmc
[    1.225032] rockchip-pinctrl pinctrl: pin-136 (fe320000.dwmmc) status -22
[    1.225660] rockchip-pinctrl pinctrl: could not request pin 136 (gpio4-8) from group sdmmc-bus4  on device rockchip-pinctrl
[    1.226657] dwmmc_rockchip fe320000.dwmmc: Error applying setting, reverse things back
[    1.227850] dwmmc_rockchip fe320000.dwmmc: IDMAC supports 32-bit address mode.
[    1.228552] dwmmc_rockchip fe320000.dwmmc: Using internal DMA controller.
[    1.229166] dwmmc_rockchip fe320000.dwmmc: Version ID is 270a
[    1.229735] dwmmc_rockchip fe320000.dwmmc: DW MMC controller at irq 26,32 bit host data width,256 deep fifo
[    1.230623] dwmmc_rockchip fe320000.dwmmc: 'clock-freq-min-max' property was deprecated.
[    1.231601] dwmmc_rockchip fe320000.dwmmc: No vmmc regulator found
[    1.233089] rockchip-iodomain ff770000.syscon:io-domains: Setting to 3300000 done
[    1.233966] rockchip-iodomain ff770000.syscon:io-domains: Setting to 3300000 done
[    1.245646] mmc_host mmc0: Bus speed (slot 0) = 400000Hz (slot req 400000Hz, actual 400000HZ div = 0)
[    1.256680] dwmmc_rockchip fe320000.dwmmc: 1 slots initialized
[    1.257759] sdhci-pltfm: SDHCI platform and OF driver helper
[    1.260223] sdhci-arasan fe330000.sdhci: No vmmc regulator found
[    1.260772] sdhci-arasan fe330000.sdhci: No vqmmc regulator found
[    1.287681] mmc1: SDHCI controller on fe330000.sdhci [fe330000.sdhci] using ADMA
[    1.289412] hidraw: raw HID events driver (C) Jiri Kosina
[    1.290619] usbcore: registered new interface driver usbhid
[    1.291117] usbhid: USB HID core driver
[    1.292442] rockchip-dmc dmc: unable to get devfreq-event device : dfi
[    1.296397] usbcore: registered new interface driver snd-usb-audio
[    1.302089] Initializing XFRM netlink socket
[    1.303128] NET: Registered protocol family 10
[    1.304631] NET: Registered protocol family 17
[    1.305053] NET: Registered protocol family 15
[    1.305957] Bluetooth: RFCOMM socket layer initialized
[    1.306443] Bluetooth: RFCOMM ver 1.11
[    1.306829] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    1.307362] Bluetooth: HIDP socket layer initialized
[    1.307884] [WLAN_RFKILL]: Enter rfkill_wlan_init
[    1.308676] [WLAN_RFKILL]: Enter rfkill_wlan_probe
[    1.309147] [WLAN_RFKILL]: wlan_platdata_parse_dt: wifi_chip_type = ap6356s
[    1.309779] [WLAN_RFKILL]: wlan_platdata_parse_dt: enable wifi power control.
[    1.310409] [WLAN_RFKILL]: wlan_platdata_parse_dt: wifi power controled by gpio.
[    1.311166] [WLAN_RFKILL]: wlan_platdata_parse_dt: get property: WIFI,host_wake_irq = 3, flags = 0.
[    1.312036] [WLAN_RFKILL]: wlan_platdata_parse_dt: The ref_wifi_clk not found !
[    1.312719] [WLAN_RFKILL]: rfkill_wlan_probe: init gpio
[    1.313195] [WLAN_RFKILL]: Exit rfkill_wlan_probe
[    1.313706] [BT_RFKILL]: Enter rfkill_rk_init
[    1.314545] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: uart_rts_gpios = 83.
[    1.315386] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: BT,reset_gpio = 92.
[    1.316125] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: BT,wake_gpio = 90.
[    1.316851] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: BT,wake_host_irq = 5.
[    1.318398] [BT_RFKILL]: Request irq for bt wakeup host
[    1.318948] [BT_RFKILL]: ** disable irq
[    1.319422] [BT_RFKILL]: bt_default device registered.
[    1.319977] Key type dns_resolver registered
[    1.321146] ov13850 1-0010: driver version: 00.01.01
[    1.321817] 1-0010 supply avdd not found, using dummy regulator
[    1.322404] 1-0010 supply dovdd not found, using dummy regulator
[    1.323010] 1-0010 supply dvdd not found, using dummy regulator
[    1.323612] ov13850 1-0010: could not get sleep pinstate
[    1.326559] ov13850 1-0010: Unexpected sensor id(000000), ret(-5)
[    1.327814] Registered cp15_barrier emulation handler
[    1.328367] Registered setend emulation handler
[    1.329425] Loading compiled-in X.509 certificates
[    1.330970] rga2: Driver loaded successfully ver:3.02
[    1.331679] rga2: Module initialized.
[    1.333165] phy phy-ff770000.syscon:usb2-phy@e450.5: Failed to get VBUS supply regulator
[    1.336014] phy phy-ff770000.syscon:usb2-phy@e460.7: Failed to get VBUS supply regulator
[    1.338020] rockchip-drm display-subsystem: defer getting devfreq
[    1.338835] rockchip-vop ff900000.vop: missing rockchip,grf property
[    1.339630] rockchip-drm display-subsystem: bound ff900000.vop (ops 0xffffff8008ac35c8)
[    1.340370] rockchip-vop ff8f0000.vop: missing rockchip,grf property
[    1.341090] rockchip-drm display-subsystem: bound ff8f0000.vop (ops 0xffffff8008ac35c8)
[    1.342049] rockchip-dp ff970000.edp: no DP phy configured
[    1.343202] i2c i2c-9: of_i2c: modalias failure on /edp@ff970000/ports
[    1.344088] rockchip-drm display-subsystem: bound ff970000.edp (ops 0xffffff8008aba580)
[    1.345145] i2c i2c-10: of_i2c: modalias failure on /hdmi@ff940000/ports
[    1.345771] dwhdmi-rockchip ff940000.hdmi: registered DesignWare HDMI I2C bus driver
[    1.346573] dwhdmi-rockchip ff940000.hdmi: Detected HDMI TX controller v2.11a with HDCP (DWC HDMI 2.0 TX PHY)
[    1.350427] rockchip-drm display-subsystem: bound ff940000.hdmi (ops 0xffffff8008ab7ec8)
[    1.352092] i2c i2c-11: of_i2c: modalias failure on /dp@fec00000/ports
[    1.353128] cdn-dp fec00000.dp: Direct firmware load for rockchip/dptx.bin failed with error -2
[    1.353947] rockchip-drm display-subsystem: bound fec00000.dp (ops 0xffffff8008ab8ee8)
[    1.354657] mmc1: MAN_BKOPS_EN bit is not set
[    1.355047] [drm] Supports vblank timestamp caching Rev 2 (21.10.2013).
[    1.355635] [drm] No driver support for vblank timestamp query.
[    1.356332] rockchip-drm display-subsystem: failed to parse loader memory
[    1.367989] mmc1: new HS400 Enhanced strobe MMC card at address 0001
[    1.370266] mmcblk1: mmc1:0001 AJTD4R 14.6 GiB 
[    1.371698] mmcblk1boot0: mmc1:0001 AJTD4R partition 1 4.00 MiB
[    1.373055] mmcblk1boot1: mmc1:0001 AJTD4R partition 2 4.00 MiB
[    1.374428] mmcblk1rpmb: mmc1:0001 AJTD4R partition 3 4.00 MiB
[    1.381156]  mmcblk1: p1 p2 p3 p4 p5 p6 p7 p8 p9
[    1.393375] rockchip-drm display-subsystem: fb0:  frame buffer device
[    1.407268] mali ff9a0000.gpu: leakage=0
[    1.434074] mali ff9a0000.gpu: temp=42777, pvtm=126008 (125996 + 12)
[    1.436646] mali ff9a0000.gpu: pvtm-volt-sel=2
[    1.438200] mali ff9a0000.gpu: avs=0
[    1.440300] mali ff9a0000.gpu: GPU identified as 0x0860 r2p0 status 0
[    1.443254] mali ff9a0000.gpu: l=0 h=2147483647 hyst=5000 l_limit=0 h_limit=0
[    1.444269] I : [File] : drivers/gpu/arm/midgard_for_linux/backend/gpu/mali_kbase_devfreq.c; [Line] : 315; [Func] : kbase_devfreq_init(); success initing power_model_simple.
[    1.447085] mali ff9a0000.gpu: Probed as mali0
[    1.454869] xhci-hcd xhci-hcd.10.auto: xHCI Host Controller
[    1.455944] xhci-hcd xhci-hcd.10.auto: new USB bus registered, assigned bus number 1
[    1.456943] xhci-hcd xhci-hcd.10.auto: hcc params 0x0220fe64 hci version 0x110 quirks 0x06030010
[    1.457859] xhci-hcd xhci-hcd.10.auto: irq 230, io mem 0xfe800000
[    1.458971] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
[    1.459627] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.460280] usb usb1: Product: xHCI Host Controller
[    1.460759] usb usb1: Manufacturer: Linux 4.4.167 xhci-hcd
[    1.461259] usb usb1: SerialNumber: xhci-hcd.10.auto
[    1.463076] hub 1-0:1.0: USB hub found
[    1.463487] hub 1-0:1.0: 1 port detected
[    1.464641] xhci-hcd xhci-hcd.10.auto: xHCI Host Controller
[    1.465642] xhci-hcd xhci-hcd.10.auto: new USB bus registered, assigned bus number 2
[    1.466495] usb usb2: We don't know the algorithms for LPM for this host, disabling LPM.
[    1.467513] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003
[    1.468177] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.468857] usb usb2: Product: xHCI Host Controller
[    1.469310] usb usb2: Manufacturer: Linux 4.4.167 xhci-hcd
[    1.469830] usb usb2: SerialNumber: xhci-hcd.10.auto
[    1.471552] hub 2-0:1.0: USB hub found
[    1.471956] hub 2-0:1.0: 1 port detected
[    1.475794] xhci-hcd xhci-hcd.10.auto: remove, state 1
[    1.476306] usb usb2: USB disconnect, device number 1
[    1.503487] xhci-hcd xhci-hcd.10.auto: Host not halted after 16000 microseconds.
[    1.504161] xhci-hcd xhci-hcd.10.auto: Host controller not halted, aborting reset.
[    1.504906] xhci-hcd xhci-hcd.10.auto: USB bus 2 deregistered
[    1.506682] xhci-hcd xhci-hcd.10.auto: remove, state 1
[    1.507661] xhci-hcd xhci-hcd.11.auto: xHCI Host Controller
[    1.508464] xhci-hcd xhci-hcd.11.auto: new USB bus registered, assigned bus number 2
[    1.509430] xhci-hcd xhci-hcd.11.auto: hcc params 0x0220fe64 hci version 0x110 quirks 0x06030010
[    1.510322] xhci-hcd xhci-hcd.11.auto: irq 231, io mem 0xfe900000
[    1.511233] usb usb2: New USB device found, idVendor=1d6b, idProduct=0002
[    1.511876] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.512551] usb usb2: Product: xHCI Host Controller
[    1.513002] usb usb2: Manufacturer: Linux 4.4.167 xhci-hcd
[    1.513500] usb usb2: SerialNumber: xhci-hcd.11.auto
[    1.514824] hub 2-0:1.0: USB hub found
[    1.515213] hub 2-0:1.0: 1 port detected
[    1.516130] xhci-hcd xhci-hcd.11.auto: xHCI Host Controller
[    1.516915] usb usb1: USB disconnect, device number 1
[    1.518026] xhci-hcd xhci-hcd.11.auto: new USB bus registered, assigned bus number 3
[    1.519220] xhci-hcd xhci-hcd.10.auto: USB bus 1 deregistered
[    1.520014] usb usb3: We don't know the algorithms for LPM for this host, disabling LPM.
[    1.521284] usb usb3: New USB device found, idVendor=1d6b, idProduct=0003
[    1.521926] usb usb3: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.522592] usb usb3: Product: xHCI Host Controller
[    1.523044] usb usb3: Manufacturer: Linux 4.4.167 xhci-hcd
[    1.523559] usb usb3: SerialNumber: xhci-hcd.11.auto
[    1.525048] hub 3-0:1.0: USB hub found
[    1.525482] hub 3-0:1.0: 1 port detected
[    1.531387] ehci-platform fe380000.usb: EHCI Host Controller
[    1.532663] ehci-platform fe380000.usb: new USB bus registered, assigned bus number 1
[    1.533646] ehci-platform fe380000.usb: irq 28, io mem 0xfe380000
[    1.539612] ehci-platform fe380000.usb: USB 2.0 started, EHCI 1.00
[    1.540627] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
[    1.541248] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.541929] usb usb1: Product: EHCI Host Controller
[    1.542383] usb usb1: Manufacturer: Linux 4.4.167 ehci_hcd
[    1.542906] usb usb1: SerialNumber: fe380000.usb
[    1.544723] hub 1-0:1.0: USB hub found
[    1.545128] hub 1-0:1.0: 1 port detected
[    1.549561] ehci-platform fe3c0000.usb: EHCI Host Controller
[    1.551052] ehci-platform fe3c0000.usb: new USB bus registered, assigned bus number 4
[    1.552017] ehci-platform fe3c0000.usb: irq 30, io mem 0xfe3c0000
[    1.558615] ehci-platform fe3c0000.usb: USB 2.0 started, EHCI 1.00
[    1.559624] usb usb4: New USB device found, idVendor=1d6b, idProduct=0002
[    1.560246] usb usb4: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.560926] usb usb4: Product: EHCI Host Controller
[    1.561380] usb usb4: Manufacturer: Linux 4.4.167 ehci_hcd
[    1.561904] usb usb4: SerialNumber: fe3c0000.usb
[    1.564233] hub 4-0:1.0: USB hub found
[    1.564688] hub 4-0:1.0: 1 port detected
[    1.567175] ohci-platform fe3a0000.usb: Generic Platform OHCI controller
[    1.568458] ohci-platform fe3a0000.usb: new USB bus registered, assigned bus number 5
[    1.569431] ohci-platform fe3a0000.usb: irq 29, io mem 0xfe3a0000
[    1.624984] usb usb5: New USB device found, idVendor=1d6b, idProduct=0001
[    1.625661] usb usb5: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.626315] usb usb5: Product: Generic Platform OHCI controller
[    1.626886] usb usb5: Manufacturer: Linux 4.4.167 ohci_hcd
[    1.627386] usb usb5: SerialNumber: fe3a0000.usb
[    1.629483] hub 5-0:1.0: USB hub found
[    1.630036] hub 5-0:1.0: 1 port detected
[    1.632422] ohci-platform fe3e0000.usb: Generic Platform OHCI controller
[    1.633688] ohci-platform fe3e0000.usb: new USB bus registered, assigned bus number 6
[    1.634634] ohci-platform fe3e0000.usb: irq 31, io mem 0xfe3e0000
[    1.690040] usb usb6: New USB device found, idVendor=1d6b, idProduct=0001
[    1.690706] usb usb6: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.691359] usb usb6: Product: Generic Platform OHCI controller
[    1.691941] usb usb6: Manufacturer: Linux 4.4.167 ohci_hcd
[    1.692441] usb usb6: SerialNumber: fe3e0000.usb
[    1.694851] hub 6-0:1.0: USB hub found
[    1.695386] hub 6-0:1.0: 1 port detected
[    1.697689] input: adc-keys as /devices/platform/adc-keys/input/input3
[    1.701368] dwmmc_rockchip fe310000.dwmmc: IDMAC supports 32-bit address mode.
[    1.702196] dwmmc_rockchip fe310000.dwmmc: Using internal DMA controller.
[    1.702855] dwmmc_rockchip fe310000.dwmmc: Version ID is 270a
[    1.703419] dwmmc_rockchip fe310000.dwmmc: DW MMC controller at irq 25,32 bit host data width,256 deep fifo
[    1.704345] dwmmc_rockchip fe310000.dwmmc: 'clock-freq-min-max' property was deprecated.
[    1.705296] dwmmc_rockchip fe310000.dwmmc: No vmmc regulator found
[    1.705920] dwmmc_rockchip fe310000.dwmmc: No vqmmc regulator found
[    1.707106] dwmmc_rockchip fe310000.dwmmc: allocated mmc-pwrseq
[    1.718780] mmc_host mmc2: Bus speed (slot 0) = 400000Hz (slot req 400000Hz, actual 400000HZ div = 0)
[    1.730924] dwmmc_rockchip fe310000.dwmmc: 1 slots initialized
[    1.733216] rockchip-dmc dmc: Failed to get leakage
[    1.733986] rockchip-dmc dmc: Failed to get pvtm
[    1.735289] rockchip-dmc dmc: avs=0
[    1.736175] rockchip-dmc dmc: l=-2147483648 h=2147483647 hyst=0 l_limit=0 h_limit=0
[    1.736918] rockchip-dmc dmc: could not find power_model node
[    1.744652] asoc-simple-card rk809-sound: rk817-hifi <-> ff890000.i2s mapping ok
[    1.745339] asoc-simple-card rk809-sound: ASoC: no source widget found for MICBIAS1
[    1.746058] asoc-simple-card rk809-sound: ASoC: Failed to add route MICBIAS1 -> direct -> Mic Jack
[    1.746878] asoc-simple-card rk809-sound: ASoC: no sink widget found for IN1P
[    1.747535] asoc-simple-card rk809-sound: ASoC: Failed to add route Mic Jack -> direct -> IN1P
[    1.748303] asoc-simple-card rk809-sound: ASoC: no source widget found for HPOL
[    1.748977] asoc-simple-card rk809-sound: ASoC: Failed to add route HPOL -> direct -> Headphone Jack
[    1.749828] asoc-simple-card rk809-sound: ASoC: no source widget found for HPOR
[    1.750482] asoc-simple-card rk809-sound: ASoC: Failed to add route HPOR -> direct -> Headphone Jack
[    1.757500] asoc-simple-card hdmi-sound: i2s-hifi <-> ff8a0000.i2s mapping ok
[    1.764629] rk808-rtc rk808-rtc: setting system clock to 2017-08-05 09:00:03 UTC (1501923603)
[    1.781612] mmc2: queuing unknown CIS tuple 0x80 (2 bytes)
[    1.783755] mmc2: queuing unknown CIS tuple 0x80 (3 bytes)
[    1.785967] mmc2: queuing unknown CIS tuple 0x80 (3 bytes)
[    1.789347] mmc2: queuing unknown CIS tuple 0x80 (7 bytes)
[    1.793353] mmc2: queuing unknown CIS tuple 0x81 (9 bytes)
[    1.804148] I : [File] : drivers/gpu/arm/mali400/mali/linux/mali_kernel_linux.c; [Line] : 417; [Func] : mali_module_init(); svn_rev_string_from_arm of this mali_ko is '', rk_ko_ver is '5', built at '11:5.
[    1.807009] Mali: Mali device driver loaded
[    1.807827] rkisp1 ff910000.rkisp1: clear unready subdev num: 1
[    1.809793] rockchip-mipi-dphy-rx: No link between dphy and sensor
[    1.810357] rkisp1 ff910000.rkisp1: failed to get fmt for rockchip-mipi-dphy-rx
[    1.811088] rkisp1 ff920000.rkisp1: clear unready subdev num: 1
[    1.812708] rkisp1: Async subdev notifier completed
[    1.813914] ALSA device list:
[    1.814219]   #0: rockchip,rk809-codec
[    1.814587]   #1: rockchip,hdmi
[    1.838902] vendor storage:20190527 ret = 0
[    1.852402] EXT4-fs (mmcblk1p8): recovery complete
[    1.853602] EXT4-fs (mmcblk1p8): mounted filesystem with ordered data mode. Opts: (null)
[    1.854467] VFS: Mounted root (ext4 filesystem) on device 179:8.
[    1.855127] mmc_host mmc2: Bus speed (slot 0) = 150000000Hz (slot req 150000000Hz, actual 150000000HZ div = 0)
[    1.860029] devtmpfs: mounted
[    1.861094] Freeing unused kernel memory: 1024K
[    2.002748] systemd[1]: Failed to insert module 'autofs4': No such file or directory
[    2.013267] random: systemd: uninitialized urandom read (16 bytes read, 52 bits of entropy available)
[    2.018772] random: systemd: uninitialized urandom read (16 bytes read, 52 bits of entropy available)

Welcome to Debian GNU/Linux 9 (stretch)!

[    2.064480] random: systemd: uninitialized urandom read (16 bytes read, 54 bits of entropy available)
[    2.090209] random: systemd-gpt-aut: uninitialized urandom read (16 bytes read, 55 bits of entropy available)
[    2.091630] random: systemd-gpt-aut: uninitialized urandom read (16 bytes read, 55 bits of entropy available)
[    2.092901] random: systemd-gpt-aut: uninitialized urandom read (16 bytes read, 55 bits of entropy available)
[    2.094813] random: systemd-gpt-aut: uninitialized urandom read (16 bytes read, 55 bits of entropy available)
[    2.095734] random: systemd-cryptse: uninitialized urandom read (16 bytes read, 55 bits of entropy available)
[    2.100978] random: systemd-sysv-ge: uninitialized urandom read (16 bytes read, 56 bits of entropy available)
[    2.105169] random: systemd-sysv-ge: uninitialized urandom read (16 bytes read, 56 bits of entropy available)
[    2.111809] dwmmc_rockchip fe310000.dwmmc: Successfully tuned phase to 212
[    2.114597] mmc2: queuing unknown CIS tuple 0x91 (3 bytes)
[    2.115151] mmc2: new ultra high speed SDR104 SDIO card at address 0001
Configuration file /lib/systemd/system/triggerhappy.service is marked executable. Please remove executable permission bits. Proceeding anyway.
[  OK  ] Reached target Remote File Systems.
[  OK  ] Reached target Swap.
[  OK  ] Listening on /dev/initctl Compatibility Named Pipe.
[  OK  ] Listening on Network Service Netlink Socket.
[  OK  ] Listening on udev Kernel Socket.
[  OK  ] Created slice User and Session Slice.
[  OK  ] Listening on Journal Socket.
[  OK  ] Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Created slice System Slice.
         Mounting Debug File System...
[  OK  ] Created slice system-getty.slice.
[  OK  ] Reached target Slices.
         Starting Set the console keyboard layout...
[  OK  ] Created slice system-serial\x2dgetty.slice.
         Starting Nameserver information manager...
[  OK  ] Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Reached target Encrypted Volumes.
         Starting Load Kernel Modules...
         Starting Remount Root and Kernel File Systems...
         Starting Create Static Device Nodes in /dev...
[  OK  ] Listening on Syslog Socket.
[  OK  ] Listening on udev Control Socket.
[  OK  ] Listening on Journal Socket (/dev/log).
         Starting Journal Service...
[  OK  ] Mounted Debug File System.
[  OK  ] Started Load Kernel Modules.
[    2.356758] cdn-dp fec00000.dp: [drm:cdn_dp_pd_event_work] Not connected. Disabling cdn
[  OK  ] Started Remount Root and Kernel File Systems.
[  OK  ] Started Nameserver information manager.
[  OK  ] Started Create Static Device Nodes in /dev.
         Starting udev Kernel Device Manager...
         Starting udev Coldplug all Devices...
         Starting Load/Save Random Seed...
         Mounting FUSE Control File System...
         Mounting Configuration File System...
         Starting Apply Kernel Variables...
[  OK  ] Mounted FUSE Control File System.
[  OK  ] Mounted Configuration File System.
[  OK  ] Started Load/Save Random Seed.
[  OK  ] Started Apply Kernel Variables.
[  OK  ] Started udev Kernel Device Manager.
         Starting Network Service...
[  OK  ] Started Network Service.
[  OK  ] Started Set the console keyboard layout.
[  OK  ] Reached target Local File Systems (Pre).
[  OK  ] Reached target Local File Systems.
         Starting Set console font and keymap...
         Starting Raise network interfaces...
[  OK  ] Started Set console font and keymap.
[  OK  ] Started Journal Service.
         Starting Flush Journal to Persistent Storage...
[    2.654197] systemd-journald[194]: Received request to flush runtime journal from PID 1
[  OK  ] Started Flush Journal to Persistent Storage.
         Starting Create Volatile Files and Directories...
[  OK  ] Started udev Coldplug all Devices.
[  OK  ] Created slice system-systemd\x2dbacklight.slice.
         Starting Load/Save Screen Backlight��…ightness of backlight:backlight...
[  OK  ] Started Create Volatile Files and Directories.
         Starting Network Time Synchronization...
         Starting Update UTMP about System Boot/Shutdown...
[  OK  ] Started Load/Save Screen Backlight Brightness of backlight:backlight.
[  OK  ] Started Update UTMP about System Boot/Shutdown.
[  OK  ] Started Network Time Synchronization.
[  OK  ] Reached target System Time Synchronized.
[  OK  ] Reached target System Initialization.
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Listening on OpenBSD Secure Shell server socket.
[  OK  ] Started Trigger anacron every hour.
[  OK  ] Started Daily apt download activities.
[  OK  ] Started Daily apt upgrade and clean activities.
[  OK  ] Reached target Timers.
[  OK  ] Listening on ACPID Listen Socket.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Reached target Sockets.
[  OK  ] Started ACPI Events Check.
[  OK  ] Reached target Paths.
[  OK  ] Reached target Basic System.
[  OK  ] Started Run anacron jobs.
         Starting Daemon for power management...
         Starting triggerhappy global hotkey daemon...
         Starting LSB: Load kernel modules needed to enable cpufreq scaling...
         Starting Disk Manager...
         Starting Provide limited super user privileges to specific users...
         Starting System Logging Service...
         Starting Login Service...
[  OK  ] Started ACPI event daemon.
         Starting Save/Restore Sound Card State...
[  OK  ] Started Setup rockchip platform environment.
[  OK  ] Started D-Bus System Message Bus.
         Starting Network Manager...
         Starting WPA supplicant...
[  OK  ] Started System Logging Service.
[FAILED] Failed to start triggerhappy global hotkey daemon.
See 'systemctl status triggerhappy.service' for details.
[  OK  ] Started Provide limited super user privileges to specific users.
[  OK  ] Started Save/Restore Sound Card State.
[  OK  ] Found device /dev/ttyFIQ0.
[  OK  ] Started WPA supplicant.
[  OK  ] Started Login Service.
         Starting Authorization Manager...
[  OK  ] Listening on Load/Save RF Kill Switch Status /dev/rfkill Watch.
[  OK  ] Reached target Sound Card.
         Starting Load/Save RF Kill Switch Status...
[    3.180609] [BT_RFKILL]: ENABLE UART_RTS
[  OK  ] Started LSB: Load kernel modules needed to enable cpufreq scaling.
         Starting LSB: set CPUFreq kernel parameters...
[  OK  ] Started Authorization Manager.
[  OK  ] Started Raise network interfaces.
[    3.281541] [BT_RFKILL]: DISABLE UART_RTS
[    3.281954] [BT_RFKILL]: bt turn on power
[  OK  ] Started Load/Save RF Kill Switch Status.
[  OK  ] Started Network Manager.
[  OK  ] Reached target Network.
[    3.292767] usb 2-1: new high-speed USB device number 2 using xhci-hcd
[  OK  ] Reached target Network is Online.
         Starting LSB: Advanced IEEE 802.11 management daemon...
         Starting Network Name Resolution...
         Starting /etc/rc.local Compatibility...
         Starting Permit User Sessions...
[  OK  ] Started LSB: Advanced IEEE 802.11 management daemon.
[  OK  ] Started Disk Manager.
[  OK  ] Started Permit User Sessions.
         Starting Light Display Manager...
         Starting Network Manager Script Dispatcher Service...
[  OK  ] Started LSB: set CPUFreq kernel parameters.
[    3.407358] usb 2-1: New USB device found, idVendor=2207, idProduct=180a
[    3.408032] usb 2-1: New USB device strings: Mfr=0, Product=0, SerialNumber=0
[    3.409361] random: nonblocking pool is initialized
[  OK  ] Started Network Manager Script Dispatcher Service.
[    3.453110] cfg80211: World regulatory domain updated:
[    3.453601] cfg80211:  DFS Master region: unset
[    3.453988] cfg80211:   (start_freq - end_freq @ bandwidth), (max_antenna_gain, max_eirp), (dfs_cac_time)
[    3.454908] cfg80211:   (2402000 KHz - 2472000 KHz @ 40000 KHz), (N/A, 2000 mBm), (N/A)
[    3.455659] cfg80211:   (2457000 KHz - 2482000 KHz @ 20000 KHz, 92000 KHz AUTO), (N/A, 2000 mBm), (N/A)
[    3.456492] cfg80211:   (2474000 KHz - 2494000 KHz @ 20000 KHz), (N/A, 2000 mBm), (N/A)
[    3.457249] cfg80211:   (5170000 KHz - 5250000 KHz @ 80000 KHz, 160000 KHz AUTO), (N/A, 2000 mBm), (N/A)
[    3.458107] cfg80211:   (5250000 KHz - 5330000 KHz @ 80000 KHz, 160000 KHz AUTO), (N/A, 2000 mBm), (0 s)
[    3.458962] cfg80211:   (5490000 KHz - 5730000 KHz @ 160000 KHz), (N/A, 2000 mBm), (0 s)
[    3.459708] cfg80211:   (5735000 KHz - 5835000 KHz @ 80000 KHz), (N/A, 2000 mBm), (N/A)
[    3.460419] cfg80211:   (57240000 KHz - 63720000 KHz @ 2160000 KHz), (N/A, 0 mBm), (N/A)
[    3.463592] dhd_module_init: in Dongle Host Driver, version 1.579.77.41.10 (r)
[    3.464251] ======== dhd_wlan_init_plat_data ========
[    3.464761] [WLAN_RFKILL]: rockchip_wifi_get_oob_irq: Enter
[    3.465266] dhd_wlan_init_gpio: WL_HOST_WAKE=-1, oob_irq=71, oob_irq_flags=0x414
[    3.465957] dhd_wlan_init_gpio: WL_REG_ON=-1
[    3.466348] dhd_wifi_platform_load: Enter
[    3.466764] Power-up adapter 'DHD generic adapter'
[    3.468312] wifi_platform_set_power = 1
[    3.468683] ======== PULL WL_REG_ON(-1) HIGH! ========
[    3.469141] [WLAN_RFKILL]: rockchip_wifi_power: 1
[    3.469607] [WLAN_RFKILL]: wifi turn on power. -1
[  OK  ] Started Light Display Manager.
[    3.496145] rockchip-vop ff900000.vop: [drm:vop_crtc_enable] Update mode to 1536x2048p60, type: 14
         Starting Hostname Service...
[    3.540821] rockchip-dp ff970000.edp: [drm:analogix_dp_link_start] Enable downspread on the sink
[    3.543576] rockchip-dp ff970000.edp: Link Training Clock Recovery success
[    3.545723] rockchip-dp ff970000.edp: Link Training success!
[  OK  ] Started Network Name Resolution.
[  OK  ] Started Hostname Service.
[    3.735251] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
[    3.783009] wifi_platform_bus_enumerate device present 1
[    3.783504] ======== Card detection to detect SDIO card! ========
[    3.784104] mmc2:mmc host rescan start!
[    3.800848] bcmsdh_register: register client driver
[    3.801505] bcmsdh_sdmmc_probe: Enter num=1
[    3.802097] bcmsdh_sdmmc_probe: Enter num=2
[    3.802483] bus num (host idx)=2, slot num (rca)=1
[    3.802976] found adapter info 'DHD generic adapter'
[    3.804908] sdioh_attach: set sd_f2_blocksize 256
[    3.805387] sdioh_attach: sd clock rate = 0
[    3.806711] dhdsdio_probe : no mutex held. set lock
[    3.808283] F1 signature read @0x18000000=0x17294359
[    3.812324] F1 signature OK, socitype:0x1 chip:0x4359 rev:0x9 pkg:0x2
[    3.813379] DHD: dongle ram size is set to 917504(orig 917504) at 0x160000
[    3.814990] dhd_conf_set_chiprev: chip=0x4359, chiprev=9
[    3.816419] CFG80211-ERROR) wl_setup_wiphy : Registering Vendor80211
[    3.819593] CFG80211-ERROR) wl_init_prof : wl_init_prof: No profile
[    3.822010] wl_escan_attach: Enter
[    3.822365] wl_escan_init: Enter
[    3.823015] wl_create_event_handler(): thread:wl_escan_handler:27a started
[    3.823069] tsk Enter, tsk = 0xffffffc0e9fb01b0
[    3.824443] dhd_attach(): thread:dhd_watchdog_thread:27d started
[    3.825339] dhd_attach(): thread:dhd_dpc:27e started
[    3.826650] dhd_attach(): thread:dhd_rxf:27f started
[    3.827139] dhd_deferred_work_init: work queue initialized
[    3.827713] dhd_tcpack_suppress_set: TCP ACK Suppress mode 0 -> mode 2
[    3.828373] dhd_bus_set_default_min_res_mask: Unhandled chip id
[    3.829295] sdioh_cis_read: func_cis_ptr[0]=0x10ac
[    3.842340] Dongle Host Driver, version 1.579.77.41.10 (r)
[    3.844467] wl_ext_iapsta_attach_netdev: ifidx=0, bssidx=0
[    3.845029] Register interface [wlan0]  MAC: cc:4b:73:3a:7a:c0
[    3.845029] 
[    3.845900] dhd_tcpack_suppress_set: TCP ACK Suppress mode 2 -> mode 0
[    3.846513] dhd_wl_ioctl: returning as busstate=0
[    3.846985] dhd_dbg_detach_pkt_monitor, 2204
[    3.847386] dhd_bus_devreset: == Power OFF ==
[    3.848124] bcmsdh_oob_intr_unregister: Enter
[    3.848550] bcmsdh_oob_intr_unregister: irq is not registered
[    3.849077] dhd_txglom_enable: enable 0
[    3.849432] dhd_conf_set_txglom_params: txglom_mode=copy
[    3.849939] dhd_conf_set_txglom_params: txglomsize=0, deferred_tx_len=0
[    3.850573] dhd_conf_set_txglom_params: txinrx_thres=128, dhd_txminmax=-1
[    3.851187] dhd_conf_set_txglom_params: tx_max_offset=0, txctl_tmo_fix=300
[    3.851850] dhd_bus_devreset:  WLAN OFF DONE
[    3.852352] wifi_platform_set_power = 0
[    3.852738] ======== PULL WL_REG_ON(-1) LOW! ========
[    3.853190] [WLAN_RFKILL]: rockchip_wifi_power: 0
[    3.853686] [WLAN_RFKILL]: wifi shut off power.
[    3.854100] dhdsdio_probe : the lock is released.
[    3.854817] dhd_module_init: Exit err=0
[    3.867305] [BT_RFKILL]: bt shut off power
[    3.878312] IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
[    3.879129] dhd_open: Enter ffffffc0f0924000
[    3.879537] dhd_open : no mutex held. set lock
[    3.880038] 
[    3.880038] Dongle Host Driver, version 1.579.77.41.10 (r)
[    3.880699] wl_ext_iapsta_attach_netdev: ifidx=0, bssidx=0
[    3.881189] wl_android_wifi_on in 1
[    3.881633] wl_android_wifi_on in 2: g_wifi_on=0
[    3.882046] wifi_platform_set_power = 1
[    3.882387] ======== PULL WL_REG_ON(-1) HIGH! ========
[    3.882860] [WLAN_RFKILL]: rockchip_wifi_power: 1
[    3.883280] [WLAN_RFKILL]: wifi turn on power. -1
[    3.914561] [BT_RFKILL]: ENABLE UART_RTS
[    4.015568] [BT_RFKILL]: DISABLE UART_RTS
[    4.016032] [BT_RFKILL]: bt turn on power
[    3.953321] rc.local[550]: Rockchip Linux WifiBt init
[    3.954683] rc.local[550]: BT TTY: /dev/ttyS0
[    3.955455] rc.local[550]: Wifi driver is not ready.
[    3.956060] rc.local[550]: wifibt_load_driver
[    3.956686] rc.local[550]: uevent path:/sys/bus/sdio/devices/./uevent
[    3.957304] rc.local[550]: uevent path:/sys/bus/sdio/devices/../uevent
[    3.958052] rc.local[550]: uevent path:/sys/bus/sdio/devices/mmc2:0001:1/uevent
[    3.958890] rc.local[550]: line: SDIO_CLASS=00
[    3.961057] rc.local[550]: , prefix: SDIO_ID=.
[    3.962637] rc.local[550]: line: SDIO_ID=02D0:4359
[    3.963795] rc.local[550]: , prefix: SDIO_ID=.
[    3.964806] rc.local[550]: pid:vid : 02d0:4359
[    3.965572] rc.local[550]: found device pid:vid : 02d0:4359
[    3.966285] rc.local[550]: wifi detectd return ret:0
[    3.967053] rc.local[550]: SDIO WIFI identify sucess
[    3.967772] rc.local[550]: check_wifi_chip_type_string: AP6398S
[    3.968763] rc.local[550]: Cannot create "/data/wifi_chip": No such file or directorywifibt_load_driver matched ko file path  /system/lib/modules/bcmdhd.ko
[    3.970484] rc.local[550]: wifibt_load_driver insmod /system/lib/modules/bcmdhd.ko
[    3.971928] rc.local[550]: Wifi driver is not ready.
[    3.973832] rc.local[550]: Wifi driver is not ready.
[    3.975644] rc.local[550]: Wifi driver is ready for now...
[    3.978113] rc.local[550]: wifibt_load_driver brcm_patchram_plus1 --bd_addr_rand --enable_hci --no2bytes --use_baudrate_for_download  --tosleep  200000 --baudrate 1500000 --patchram  /system/etc/firmware&
[  OK  ] Started Daemon for power management.
[    3.985632] rc.local[550]: cp: cannot stat '/data/cfg/device_info.txt': No such file or directory
[    4.061212] ttyS0 - failed to request DMA, use interrupt mode
[  OK  ] Started /etc/rc.local Compatibility.
[  OK  ] Started Serial Getty on ttyFIQ0.
[  OK  ] Started Getty on tty1.
[  OK  ] Reached target Login Prompts.
[  OK  ] Reached target Multi-User System.
[  OK  ] Reached target Graphical Interface.
         Starting Update UTMP about System Runlevel Changes...
[  OK  ] Started Update UTMP about System Runlevel Changes.
[    4.125316] tty_port_close_start: tty->count = 1 port count = 2.

Debian GNU/Linux 9 linaro-alip ttyFIQ0

linaro-alip login: root (automatic login)

Last login: Wed Oct  2 08:48:50 UTC 2019 on ttyFIQ0
[    4.172813] usb 2-1: reset high-speed USB device number 2 using xhci-hcd
Linux linaro-alip 4.4.167 #1 SMP Thu Aug 22 11:58:03 KST 2019 aarch64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
[    4.184974] sdio_reset_comm():
[    4.200648] mmc_host mmc2: Bus speed (slot 0) = 400000Hz (slot req 400000Hz, actual 400000HZ div = 0)
[    4.218688] mmc_host mmc2: Bus speed (slot 0) = 200000Hz (slot req 200000Hz, actual 200000HZ div = 0)
[    4.260136] mmc2: queuing unknown CIS tuple 0x80 (2 bytes)
[    4.263719] mmc2: queuing unknown CIS tuple 0x80 (3 bytes)
[    4.267255] mmc2: queuing unknown CIS tuple 0x80 (3 bytes)
[    4.270912] [drm:dw_hdmi_rockchip_set_property] *ERROR* failed to set rockchip hdmi connector property
[    4.271766] [drm:dw_hdmi_rockchip_set_property] *ERROR* failed to set rockchip hdmi connector property
[    4.272631] [drm:dw_hdmi_rockchip_set_property] *ERROR* failed to set rockchip hdmi connector property
[    4.273445] mmc2: queuing unknown CIS tuple 0x80 (7 bytes)
[    4.273974] [drm:dw_hdmi_rockchip_set_property] *ERROR* failed to set rockchip hdmi connector property
[    4.274837] [drm:dw_hdmi_rockchip_set_property] *ERROR* failed to set rockchip hdmi connector property
[    4.275691] [drm:dw_hdmi_rockchip_set_property] *ERROR* failed to set rockchip hdmi connector property
[    4.289597] usb 2-1: device descriptor read/64, error -71
[    4.329611] dwmmc_rockchip fe310000.dwmmc: Unexpected interrupt latency
[    4.400375] mmc_host mmc2: Bus speed (slot 0) = 150000000Hz (slot req 150000000Hz, actual 150000000HZ div = 0)
root@linaro-alip:~# [    4.515876] usb 2-1: device firmware changed
[    4.517358] usb 2-1: USB disconnect, device number 2
[    4.624686] usb 2-1: new high-speed USB device number 3 using xhci-hcd
[    4.653922] dwmmc_rockchip fe310000.dwmmc: Successfully tuned phase to 211
[    4.654739] sdioh_start: set sd_f2_blocksize 256
[    4.655507] 
[    4.655507] 
[    4.655507] dhd_bus_devreset: == WLAN ON ==
[    4.656367] F1 signature read @0x18000000=0x17294359
[    4.660377] F1 signature OK, socitype:0x1 chip:0x4359 rev:0x9 pkg:0x2
[    4.661496] DHD: dongle ram size is set to 917504(orig 917504) at 0x160000
[    4.662326] dhd_bus_set_default_min_res_mask: Unhandled chip id
[    4.663249] dhd_conf_read_config: Ignore config file /system/etc/firmware/config.txt
[    4.664064] Final fw_path=/system/etc/firmware/fw_bcm4359c0_ag.bin
[    4.664720] Final nv_path=/system/etc/firmware/nvram_ap6398s.txt
[    4.665302] Final clm_path=/system/etc/firmware/clm.blob
[    4.665875] Final conf_path=/system/etc/firmware/config.txt
[    4.669629] dhd_os_open_image: /system/etc/firmware/fw_bcm4359c0_ag.bin (604169 bytes) open success
[    4.740653] usb 2-1: New USB device found, idVendor=2207, idProduct=180a
[    4.741362] usb 2-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[    4.742227] usb 2-1: Product: USB-MSC
[    4.742654] usb 2-1: Manufacturer: RockChip
[    4.743045] usb 2-1: SerialNumber: rockchip
[    4.800095] dhd_os_open_image: /system/etc/firmware/nvram_ap6398s.txt (5861 bytes) open success
[    4.802294] NVRAM version: AP6398S_NVRAM_V1.1_20170926
[    4.804248] dhdsdio_write_vars: Download, Upload and compare of NVRAM succeeded.
[    4.980752] dhd_bus_init: enable 0x06, ready 0x06 (waited 0us)
[    4.981368] si_get_pmu_reg_addr: addrRET: 18000670
[    4.983107] bcmsdh_oob_intr_register: HW_OOB irq=71 flags=0x4
[    4.983993] dhd_get_memdump_info: File [/data/misc/wifi/.memdump.info] doesn't exist
[    4.984813] dhd_get_memdump_info: MEMDUMP ENABLED = 2
[    4.987301] Disable tdls_auto_op failed. -1
[    4.987755] dhd_tcpack_suppress_set: TCP ACK Suppress mode 0 -> mode 1
[    4.989418] dhd_apply_default_clm: Ignore clm file /system/etc/firmware/clm.blob
[    4.993325] Firmware up: op_mode=0x0405, MAC=cc:4b:73:3a:7a:c0
[    5.003804]   Driver: 1.579.77.41.10 (r)
[    5.003804]   Firmware: wl0: Aug  6 2018 16:57:33 version 9.87.51.11.18 (4ce23ed@shgit) (r) FWID 01-c6593a53
[    5.003804]   CLM: 9.7.5 
[    5.005690] dhd_txglom_enable: enable 1
[    5.006081] dhd_conf_set_txglom_params: txglom_mode=copy
[    5.006621] dhd_conf_set_txglom_params: txglomsize=36, deferred_tx_len=0
[    5.007254] dhd_conf_set_txglom_params: txinrx_thres=128, dhd_txminmax=-1
[    5.007912] dhd_conf_set_txglom_params: tx_max_offset=0, txctl_tmo_fix=300
[    5.008564] dhd_conf_get_disable_proptx: fw_proptx=0, disable_proptx=1
[    5.010702] dhd_pno_init: Support Android Location Service
[    5.037458] rtt_do_get_ioctl: failed to send getbuf proxd iovar (CMD ID : 1), status=-23
[    5.038246] dhd_rtt_init : FTM is not supported
[    5.038724] dhd_preinit_ioctls: SensorHub diabled 0
[    5.040149] dhd_preinit_ioctls failed to set ShubHub disable
[    5.043339] failed to set WNM capabilities
[    5.044080] dhd_conf_set_country: set country CN, revision 38
[    5.047883] Country code: CN (CN/38)
[    5.050360] CONFIG-ERROR) dhd_conf_set_intiovar: txbf setting failed -23
[    5.051477] wl_android_wifi_on: Success
[    5.075375] dhd_open : the lock is released.
[    5.075794] dhd_open: Exit ret=0
[    5.131147] P2P interface registered
[    5.131508] wl_cfgp2p_add_p2p_disc_if: wdev: ffffffc0f008f000, wdev->net:           (null)
[    5.152698] WLC_E_IF: NO_IF set, event Ignored
[    5.154633] P2P interface started
[    5.194502] wl_run_escan: LEGACY_SCAN sync ID: 0, bssidx: 0
[    5.461304] rc.local[550]: proc_resetsend hci_download_minidriverproc_resetDone setting line discpline
[    6.449655] wl_run_escan: LEGACY_SCAN sync ID: 1, bssidx: 0
[    6.553438] dwhdmi-rockchip ff940000.hdmi: Rate 0 missing; compute N dynamically
[    6.569220] dwhdmi-rockchip ff940000.hdmi: Rate 0 missing; compute N dynamically
[    7.691961] Connecting with 92:9f:33:d8:a3:32 ssid "DEV1/SW(5G)", len (11) channel=149
[    7.691961] 
[    7.696011] dhd_dbg_start_pkt_monitor, 1724
[    7.751179] wl_iw_event: Link UP with 92:9f:33:d8:a3:32
[    7.751814] wl_bss_connect_done succeeded with 92:9f:33:d8:a3:32 
[    7.753227] CFG80211-ERROR) wl_cfg80211_scan_abort : scan abort  failed 
[    7.952739] wl_bss_connect_done succeeded with 92:9f:33:d8:a3:32 vndr_oui: 00-90-4C 00-0C-43 
[    8.197809] usb 2-1: USB disconnect, device number 3
[    8.715212] IPv6: wlan0: IPv6 duplicate address fe80::82d8:7a32:1f42:ccb detected!
[    9.342917] IPv6: wlan0: IPv6 duplicate address fe80::7649:7e53:eb57:8a1 detected!
[    9.503928] EXT4-fs (mmcblk1p9): mounting ext2 file system using the ext4 subsystem
[    9.515204] EXT4-fs (mmcblk1p9): warning: mounting unchecked fs, running e2fsck is recommended
[    9.519001] EXT4-fs (mmcblk1p9): mounted filesystem without journal. Opts: (null)
[    9.547860] file system registered
[    9.568831] EXT4-fs (mmcblk1p7): mounting ext2 file system using the ext4 subsystem
[    9.570843] EXT4-fs (mmcblk1p7): warning: mounting unchecked fs, running e2fsck is recommended
[    9.572861] EXT4-fs (mmcblk1p7): mounted filesystem without journal. Opts: (null)
[    9.579867] read descriptors
[    9.580140] read strings
[    9.694821] usb 3-1: new SuperSpeed USB device number 2 using xhci-hcd
[    9.706958] usb 3-1: New USB device found, idVendor=2207, idProduct=1808
[    9.707584] usb 3-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[    9.708211] usb 3-1: Product: rk3xxx
[    9.708614] usb 3-1: Manufacturer: rockchip
[    9.708990] usb 3-1: SerialNumber: 0123456789ABCDEF
[    9.882873] [BT_RFKILL]: ENABLE UART_RTS
[    9.983603] [BT_RFKILL]: DISABLE UART_RTS
[    9.984078] [BT_RFKILL]: bt turn on power
[   10.294098] IPv6: wlan0: IPv6 duplicate address fe80::1189:aff9:1635:c408 detected!

```