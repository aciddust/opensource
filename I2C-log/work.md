# 작업일지

## 월 - 11.25

ADC 보드를 붙이기위해 패치를 요청함.  
ADC 보드 한 대는 정상작동하나 받아온 보드와 함께 붙여보려니 안됨.  
데이터시트 참조하여 레지스터 설정을 어떻게해야하는지 검토함.  

## 화 - 11.26 

아무리 봐도 레지스터 설정문제는 아니다싶어 오실로스코프로 찍어봄.  
GPIO IC에서 SW Enable 신호는 나가는데  
뒤에 달린 스위치 쪽이 제대로 작동하고있는지  
혹은 2번째 보드의 GPIO IC가 제대로 작동하는지 알 수 없었음.  

## 수 - 11.27

2번째 GPIO IC에서 SW Enable 신호가 안나감.  
IC가 죽었나 예상해봄.  
혹시몰라서 보드 새로 하나 만들어달라고 요청함.  

새로 만들어온 보드로 붙이니 두대 다 연결됨.  
아이고 좋다 i2c버스에 잡혀있는 장치들 다 덤프떠놓음  

하는김에 4대 만들어달라고함.

ADC보드 4대 다 꼽아놓고 순서대로 레지스터 설정해보니  
2대까진 되는데 `0x03`에 장치가 잡혔다 사라진다거나,  
3번째 보드 이전에 잡힌 장치가 버스에서 하나씩 사라지다못해  
마지막엔 한개 ADC보드만 버스에 남아있음.  

> 아 뭐야 제발...

왜이러지? 풀업레지스터? 이건또 뭐야.  
퇴근함.  

## 목 - 11.28

출근해서 보드 4대를 한번에 다 장착하지 않고  
한대 장착하고 설정하고 또 장착하고 설정하고..  
이런식으로 4대를 다 장착해보니 I2C버스에서 총 8개의 장치를 확인할 수 있었음.  

어쨋든 4대의 보드가 연결되었는데 실제 사용시 이 방법은 의미가 없다고 판단.  

그래서 다시 돌아와서 어제 2번째 보드 설정 이후 3번째 설정부터 장치가 하나씩 사라지는 현상을 생각해보았는데.. 아래와 같은 의심을 하게됨.  

- 회로상의 문제로 전원 넣자마자 한번에 여러장치 다 동일한 주소로 잡히나?
- 처음엔 `0x20`이지만 그게 대체 물리적으로 어디있는 보드인가?
  - 대체 어느 보드부터 초기화가 되는것인가?
    - LED 레지스터를 건드려서 확인해보자.

확인해보니 더미보드에 꼽힌걸 기준으로 첫번째 보드가아니라  
맨 마지막에 꼽힌 네번째 보드가 0x20으로 잡히고 거기부터 초기화가 진행됨..  

아 모르겠다..  

## 금 - 11.29

ADC 보드에 전원이 들어갈때 신호부분이 문제가되는지 알기위해
레지스터 상태를 확인해보기로함.

보드 레지스터 초기값 확인

```bash
# [BEFORE] GPIO DIRECTION
sudo i2cget -y 2 0x20 0x00
>> 0xff

# [BEFORE] GPIO STATUS
sudo i2cget -y 2 0x20 0x09
>> 0x64

# [AFTER] GPIO DIRECTION
sudo i2cset -y 2 0x20 0x00 0xc8 (with led ctrl)
sudo i2cget -y 2 0x20 0x00
>> 0xc8
sudo i2cset -y 2 0x20 0x00 0xd8
sudo i2cget -y 2 0x20 0x00
>> 0xd8

# [AFTER] GPIO STATUS
sudo i2cget -y 2 0x20 0x09
>> 0x40
```

- 보드 부팅시 GPIO(0x09)는 `0x64`
- GPIO DIRECTION 설정 이후 GPIO는 `0x40`으로 바뀜
    - S/W Enable 비트가 스위칭되버림
    - DIRECTION 설정을 나눠서 해보기로함.
    - S/W Enable GPIO를 ADC리셋 이후 설정


된다ㅏ아ㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏ

```bash
#!/bin/bash

clear

# 1
sudo i2cset -y 2 0x20 0x00 0xcc
sleep 0.1
sudo i2cset -y 2 0x20 0x09 0x31
sleep 0.1
sudo i2cset -y 2 0x21 0x00 0xc8
sleep 0.1

# 2
sudo i2cset -y 2 0x20 0x00 0xcc
sleep 0.1
sudo i2cset -y 2 0x20 0x09 0x32
sleep 0.1
sudo i2cset -y 2 0x22 0x00 0xc8
sleep 0.1

# 3
sudo i2cset -y 2 0x20 0x00 0xcc
sleep 0.1
sudo i2cset -y 2 0x20 0x09 0x33
sleep 0.1
sudo i2cset -y 2 0x23 0x00 0xc8
sleep 0.1

# 4
sudo i2cset -y 2 0x20 0x00 0xcc
sleep 0.1
sudo i2cset -y 2 0x20 0x09 0x30
sleep 0.1

sudo i2cdetect -y 2
```







