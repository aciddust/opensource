EVB에서 실행가능한 `npu_powerctrl`의 원본 소스코드는 SDK의 `npu/buildroot/package/rockchip/npu_powerctrl/npu_powerctrl.c` 파일이다.  

기능별 함수 구현 되어있음.  
```c
void npu_power_gpio_init(void);
void npu_power_gpio_exit(void);
void npu_reset(void);
int npu_suspend(void);
int npu_resume(void);
void npu_poweroff(void);
```

클럭과 GPIO 설정부분.  
```c
#define GPIO_BASE_PATH "/sys/class/gpio"
#define GPIO_EXPORT_PATH GPIO_BASE_PATH "/export"
#define GPIO_UNEXPORT_PATH GPIO_BASE_PATH "/unexport"
#define CLKEN_24M_PATH "/sys/kernel/debug/clk/clk_wifi_pmu/clk_enable_count"
#define CLKEN_32k_PATH "/sys/kernel/debug/clk/rk808-clkout2/clk_enable_count"

#define NPU_VDD_0V8_GPIO    "4"  //GPIO0_PA4
#define NPU_VDD_LOG_GPIO    "10" //GPIO0_PB2
#define NPU_VCC_1V8_GPIO    "11" //GPIO0_PB3
#define NPU_VCC_DDR_GPIO    NPU_VCC_1V8_GPIO
#define NPU_VDD_CPU_GPIO    "54" //GPIO1_PC6
#define NPU_VCCIO_3V3_GPIO  "55" //GPIO1_PC7
#define NPU_VDD_GPIO        "56" //GPIO1_PD0

#define CPU_RESET_NPU_GPIO  "32" //GPIO1_PA0
#define NPU_PMU_SLEEP_GPIO  "35" //GPIO1_A3
#define CPU_INT_NPU_GPIO    "36" //GPIO1_A4
```

NPU 리셋
1. 클럭 공급 중단
2. Power Enable 신호 해제
3. 인터럽트 해제
4. Power Enable 신호 1~3 활성화
5. 클럭 공급 재개
6. Power Enable 신호 4~6 활성화
7. NPU리셋 신호 인가
```c
void npu_reset(void) {
//  sysfs_write("/sys/power/wake_lock", "npu_lock");
    sysfs_write(CLKEN_32k_PATH, "1");
    clk_enable("0");
    /*power off*/
    set_gpio(NPU_VDD_LOG_GPIO, "0");
    /* wait for usb disconnect */
    usleep(2000);
    set_gpio(NPU_VDD_GPIO, "0");
    set_gpio(NPU_VCCIO_3V3_GPIO, "0");
    set_gpio(NPU_VDD_CPU_GPIO, "0");
    set_gpio(NPU_VCC_1V8_GPIO, "0");
    set_gpio(NPU_VDD_0V8_GPIO, "0");
    set_gpio(CPU_INT_NPU_GPIO, "0");
    set_gpio(CPU_RESET_NPU_GPIO, "0");
    usleep(2000);

    /*power en*/
    set_gpio(NPU_VDD_0V8_GPIO, "1");
    usleep(2000);
    set_gpio(NPU_VDD_LOG_GPIO, "1");
    usleep(2000);
    set_gpio(NPU_VCC_1V8_GPIO, "1");
    usleep(2000);
    clk_enable("1");
    set_gpio(NPU_VDD_CPU_GPIO, "1");
    usleep(2000);
    set_gpio(NPU_VCCIO_3V3_GPIO, "1");
    usleep(2000);
    set_gpio(NPU_VDD_GPIO, "1");

    usleep(25000);
    set_gpio(CPU_RESET_NPU_GPIO, "1");
}
```

NPU 전원 내림
```c
void npu_poweroff(void) {
    /*power off*/
    set_gpio(NPU_VDD_LOG_GPIO, "0");
    /* wait for usb disconnect */
    usleep(2000);
    set_gpio(NPU_VDD_GPIO, "0");
    set_gpio(NPU_VCCIO_3V3_GPIO, "0");
    set_gpio(NPU_VDD_CPU_GPIO, "0");
    set_gpio(NPU_VCC_1V8_GPIO, "0");
    set_gpio(NPU_VDD_0V8_GPIO, "0");
    set_gpio(CPU_INT_NPU_GPIO, "0");
    set_gpio(CPU_RESET_NPU_GPIO, "0");
    clk_enable("0");
//  sysfs_write("/sys/power/wake_unlock", "npu_lock");
}
```