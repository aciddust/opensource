제공되는 예제에서 `rknn inference` 이후 데이터

```python
RESULTS = 1917
CLASSES = 91
outputs = [[len(7668)],[len(174447)]] 
predictions = outputs[0].reshape(1,RESLTS,4) # (1*1917*4)
outputClasses = outputs[1].reshape(1, RESULTS, CLASSES) # (1*1917*91)
candidateBox = np.zeros([2, RESULTS], int) # (2*1917)
BoxPriors = load_box_priors() # (4*1917)
```

---
box_priors.txt 의 숫자들은
4조각으로 나뉘어지며
각 조각은 y, x, y-ratio, x-ratio임.
---
```python
box_priors[0][0] # y
box_priors[1][0] # x
box_priors[2][0] # y-ratio
box_priors[3][0] # x-ratio
```

