# RK3399Pro SDK for `Linux`
> 2019년 06월 18일 1.0버전 릴리즈되어 다시 작성한 문서.  


## 의존성 해결
빌드하려는 Rootfs에 따라 필요한 패키지가 다름.  
아래의 패키지들을 먼저 설치하고 진행하는 것을 권장함.  

### Buildroot
```bash
# 업데이트
sudo apt-get update

# 패키지 설치
sudo apt-get install repo git-core gitk git-gui \
gcc-arm-linux-gnueabihf u-boot-tools device-tree-compiler \
gcc-aarch64-linux-gnu mtools parted libudev-dev libusb-1.0-0-dev \
python-linaro-image-tools linaro-image-tools autoconf \
autotools-dev libsigsegv2 m4 intltool libdrm-dev curl sed make \
binutils build-essential gcc g++ bash patch gzip bzip2 perl tar cpio python \
unzip rsync file bc wget libncurses5 libqt4-dev libglib2.0-dev libgtk2.0-dev \
libglade2-dev cvs git mercurial rsync openssh-client subversion asciidoc \
w3m dblatex graphviz python-matplotlib libc6:i386 libssl-dev texinfo \
liblz4-tool genext2fs
```
### Debian
```bash
# 업데이트
sudo apt-get update

# 패키지 설치
sudo apt-get install repo git-core gitk git-gui \
gcc-arm-linux-gnueabihf u-boot-tools device-tree-compiler \
gcc-aarch64-linux-gnu mtools parted libudev-dev libusb-1.0-0-dev \
python-linaro-image-tools linaro-image-tools \
gcc-4.8-multilib-arm-linux-gnueabihf gcc-arm-linux-gnueabihf libssl-dev \
gcc-aarch64-linux-gnu g+conf autotools-dev libsigsegv2 m4 intltool \
libdrm-dev curl sed make binutils build-essential gcc g++ bash \
patch gzip bzip2 perl tar cpio python unzip rsync file bc wget \
libncurses5 libqt4-dev libglib2.0-dev libgtk2.0-dev libglade2-dev \
cvs git mercurial rsync openssh-client subversion asciidoc w3m \
dblatex graphviz python-matplotlib libc6:i386 libssl-dev \
texinfo liblz4-tool genext2fs
```

## 플래싱 도구 설치
보드에 이미지 플래싱 할 때 필요한 툴을 미리 설치함.  
```bash
sudo cp tools/linux/Linux_Upgrade_Tool/Linux_Upgrade_Tool/upgrade_tool /usr/bin/
```


## Quick Start
SDK 루트 디렉터리에서 `./build.sh` 실행하면  
uboot, kernel, rootfs, recovery 이미지가 생성되고 `rockdev` 디렉터리에 각각의 이미지에대한 심볼릭 링크 및 `update.img` 파일이 생성됨.  
> 기본적으로 `buildroot` rootfs를 생성함.  
> debian rootfs를 사용하기위해 `${SDK_ROOT}/debian`에서 별도로 빌드해줘야함.  
> **관련 내용은 `Step by Step - RK3399Pro 이미지 생성 - rootfs - debian` 절을 참조. **

``` bash
cd ${SDK_ROOT} && sudo ./build.sh
sudo upgrade_tool ul MiniLoaderAll.bin
sudo upgrade_tool di -p parameter.txt
sudo upgrade_tool di -u uboot.img
sudo upgrade_tool di -t trust.img
sudo upgrade_tool di -misc misc.img
sudo upgrade_tool di -b boot.img
sudo upgrade_tool di -recovery recovery.img
sudo upgrade_tool di -oem oem.img
sudo upgrade_tool di -rootfs rootfs.img
sudo upgrade_tool di -userdata userdata.img
sudo upgrade_tool rd # 보드 재부팅
```

### 각각의 파티션 빌드
`${SDK_ROOT}/build.sh`를 사용하면 편함.  
```bash
function usage()
{
  echo "Usage: build.sh [OPTIONS]"
  echo "Available options:"
  echo "BoardConfig*.mk    -switch to specified board config"
  echo "uboot              -build uboot"
  echo "kernel             -build kernel"
  echo "modules            -build kernel modules"
  echo "rootfs             -build default rootfs, currently build buildroot as default"
  echo "buildroot          -build buildroot rootfs"
  echo "ramboot            -build ramboot image"
  echo "multi-npu_boot     -build boot image for multi-npu board"
  echo "yocto              -build yocto rootfs"
  echo "debian             -build debian9 stretch rootfs"
  echo "distro             -build debian10 buster rootfs"
  echo "pcba               -build pcba"
  echo "recovery           -build recovery"
  echo "all                -build uboot, kernel, rootfs, recovery image"
  echo "cleanall           -clean uboot, kernel, rootfs, recovery"
  echo "firmware           -pack all the image we need to boot up system"
  echo "updateimg          -pack update image"
  echo "otapackage         -pack ab update otapackage image"
  echo "save               -save images, patches, commands used to debug"
  echo "allsave            -build all & firmware & updateimg & save"
  echo ""
  echo "Default option is 'allsave'."
}
```

## Step by Step
`./build`를 실행할 경우 중간 진행과정을 순서대로 기록하였음.  

SDK 전체 빌드 및 설치확인 진행순서 (개략적)
1. RK3399Pro NPU 이미지 생성
 - (uboot -> kernel -> packaging)
2. RK3399Pro 이미지 생성
 - (uboot -> kernel -> recovery -> rootfs -> packaging)
3. 플래싱

### RK3399Pro NPU 이미지 생성
#### uboot
``` bash
cd ${SDK_ROOT}/npu/u-boot
sudo ./make.sh rknpu-lion
```

#### kernel
```bash
cd ${SDK_ROOT}/npu/kernel
sudo make ARCH=arm64 rk3399pro_npu_defconfig
sudo make ARCH=arm64 rk3399pro-npu-evb-v10.img -j12
```

#### packaging
```bash
cd ${SDK_ROOT}/npu
sudo ./build.sh ramboot
sudo ./mkfirmware rockchip_rk3399pro-npu
```

### RK3399Pro 이미지 생성
#### uboot
```bash
cd ${SDK_ROOT}/u-boot
sudo ./make.sh rk3399pro
```

#### kernel
```bash
cd ${SDK_ROOT}/kernel
sudo make ARCH=arm64 rockchip_linux_defconfig
sudo make ARCH=arm64 rk3399pro-evb-v11-linux.img -j12
```

#### recovery
```bash
cd ${SDK_ROOT}
sudo ./build.sh recovery
```

#### rootfs
##### buildroot
```bash
cd ${SDK_ROOT}
sudo ./build.sh rootfs
```

##### debian
```bash
sudo apt-get install binfmt-support qemu-user-static live-build
sudo dpkg -i ubuntu-build-service/packages/*
# 위의 패키지 설치시 의존성관련 에러가 나타나서 중단되는데 force install 시켜버림.
sudo apt-get install -f

sudo ./mk-base-debian.sh RELEASE=stretch TARGET=desktop ARCH=arm64 
sudo ./mk-rootfs.sh RELEASE=stretch ARCH=arm64
sudo ./mk-rootfs-stretch-arm64.sh VERSION=debug ARCH=arm64
sudo ./mk-image.sh
```
위의 스크립트 실행 후 `linaro-rootfs.img` 파일 생성됨.

### 심볼릭 링크 생성
지금까지 빌드한 이미지들의 심볼릭링크를 `${SDK_ROOT}/rockdev`에 생성함.  

```bash
cd ${SDK_ROOT}

# buildroot rootfs 사용시 실행
sudo ./build.sh BoardConfig.mk

# debian rootfs 사용시 실행
sudo ./build.sh BoardConfig_debian.mk

sudo ./mkfirmware.sh
```

#### 버그
현재 debian rootfs 설정이 불완전함. [BoardConfig_debian.mk]  
**그래서 직접 링크해줘야함.**  

```bash
sudo rm -rf ./rootfs.img
cd ${SDK_ROOT}/rockdev
sudo ln -s ../debian/linaro-rootfs.img ./rootfs.img
```

### 이미지 패키징
`rockdev`에 생성된 심볼릭링크 파일들을 참조하여 전체 이미지를 묶는 작업.  
필요 없을 경우 생략해도 플래싱에는 지장 없음.  

`${SDK_ROOT}/rockdev` 디렉터리에 `update.img` 생성됨.  
```bash
cd ${SDK_ROOT}
sudo ./build.sh updateimg
```

### 이미지 플래싱
```bash
cd ${SDK_ROOT}/rockdev
sudo upgrade_tool ul MiniLoaderAll.bin
sudo upgrade_tool di -p parameter.txt
sudo upgrade_tool di -u uboot.img
sudo upgrade_tool di -t trust.img
sudo upgrade_tool di -misc misc.img
sudo upgrade_tool di -b boot.img
sudo upgrade_tool di -recovery recovery.img
sudo upgrade_tool di -oem oem.img
sudo upgrade_tool di -rootfs rootfs.img
sudo upgrade_tool di -userdata userdata.img
sudo upgrade_tool rd # 보드 재부팅
```

## Tip
### 파티션 크기 변경
Debian rootfs를 예시로 기록함.
`${SDK_ROOT}/rockdev/parameter.txt` 파일의 `CMDLINE:`부분을 수정하고 플래싱할 때 사용하면 됨.  
                                                                                                                            
```txt
CMDLINE: mtdparts=rk29xxnand:0x00002000@0x00004000(uboot),0x00002000@0x00006000(trust),0x00002000@0x00008000(misc),0x00010000@0x0000a000(boot),0x00030000@0x0001a000(recovery),0x00010000@0x0004a000(backup),0x00020000@0x0005a000(oem),0x01400000@0x0007a000(rootfs),-@0x0147a000(userdata:grow)
```