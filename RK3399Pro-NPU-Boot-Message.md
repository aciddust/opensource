```bash
DDR Version V1.02 20190312
LPDDR3
933MHz
BW=32 Col=10 Bk=8 CS0 Row=15 CS1 Row=15 CS=2 Die BW=32 Size=2048MB
out
Returning to boot ROM...
Boot1 Release Time: May 21 2019 20:12:00, version: 1.03
ChipType = 0x14, 168
SecureInit ret = 0, SecureMode = 0
atags_set_bootdev: ret:(0)
NPU UsbBoot...
UsbHook ...10506
powerOn 12728
USBParse: cmd:0x18, sub_code:0xaa, len:56
MAGIC: 4d505352
0 Type:2, Addr:0x8000000
UbootAddr:0x8000000
1 Type:1, Addr:0x8100000
TrustAddr:0x8100000
2 Type:4, Addr:0x4000000
BootAddr:0x4000000
atags_set_boot_addr: ret:(0)
USBParse:ret:0
USBParse: cmd:0x19, sub_code:0xaa, len:0
LoadTrust Addr:0x40800
No find bl30.bin
No find bl32.bin
Load uboot, ReadLba = 40000
Load OK, addr=0x600000, size=0x41eb0
USB Boot OK!
RunBL31 0x10000
INFO:    Preloader serial: 2
NOTICE:  BL31: v1.3(debug):301942b
NOTICE:  BL31: Built : 17:24:17, May 21 2019
NOTICE:  BL31: Rockchip release version: v1.0
INFO:    GICv3 with legacy support detected. ARM GICV3 driver initialized in EL3
INFO:    boot from ram
INFO:    Using opteed sec cpu_context!
INFO:    boot cpu mask: 0
INFO:    plat_rockchip_pmu_init(1596): pd status 8002
INFO:    BL31: Initializing runtime services
WARNING: No OPTEE provided by BL2 boot loader, Booting device without OPTEE initialization. SMC`s destined for OPTEE will return SMC_UNK
ERROR:   Error initializing runtime service opteed_fast
INFO:    BL31: Preparing for EL3 exit to normal world
INFO:    Entry point address = 0x600000
INFO:    SPSR = 0x3c9


U-Boot 2017.09-02925-g6999767 (Jun 04 2019 - 09:36:19 +0800)

Model: Rockchip RK3399PRO NPU
PreSerial: 2
DRAM:  2 GiB
Sysmem: init
Relocation Offset is: 7f9a7000
Using default environment

Bootdev(atags): ramdisk 0
PartType: RKPARM
get part misc fail -1
boot mode: normal
Load FDT from boot part
DTB: rk-kernel.dtb
I2c speed: 100000Hz
vdd_npu 1393750 uV
Model: Rockchip RK3399pro-npu EVB V10 Board
CLK: (sync kernel. arm: enter 1200000 KHz, init 1200000 KHz, kernel 0N/A)
  apll 1200000 KHz
  dpll 462000 KHz
  cpll 1000000 KHz
  gpll 1188000 KHz
  npll 24000 KHz
  ppll 100000 KHz
  hsclk_bus 297000 KHz
  msclk_bus 198000 KHz
  lsclk_bus 99000 KHz
  msclk_peri 198000 KHz
  lsclk_peri 99000 KHz
Hit key to stop autoboot('CTRL+C'):  0 
Could not find misc partition
ANDROID: reboot reason: "(none)"
Fdt Ramdisk skip relocation
FDT load addr 0x10f00000 size 66 KiB
Booting IMAGE kernel at 0x00280000 with fdt at 0x1f00000...


## Booting Android Image at 0x0027f800 ...
Kernel load addr 0x00280000 size 3709 KiB
RAM disk load addr 0x0a200000 size 13788 KiB
## Flattened Device Tree blob at 01f00000
   Booting using the fdt blob at 0x1f00000
   XIP Kernel Image ... OK
   Using Device Tree in place at 0000000001f00000, end 0000000001f0a853
Adding bank: 0x00000000 - 0x00010000 (size: 0x00010000)
Adding bank: 0x00200000 - 0x80000000 (size: 0x7fe00000)
Total: 57.116 ms

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 4.4.167 (wxt@SYS3) (gcc version 6.3.1 20170404 (Linaro GCC 6.3-2017.05) ) #14 SMP PREEMPT Mon Jun 3 12:02:45 CST 2019
[    0.000000] Boot CPU: AArch64 Processor [410fd042]
[    0.000000] earlycon: Early serial console at MMIO32 0xff550000 (options '')
[    0.000000] bootconsole [uart0] enabled
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.0 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] PERCPU: Embedded 18 pages/cpu @ffffffc07ffa0000 s34272 r8192 d31264 u73728
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 515600
[    0.000000] Kernel command line: storagemedia=ramdisk androidboot.mode=normal androidboot.slot_suffix= androidboot.serialno=c3d9b8674f4b94f6  earlycon=uart8250,mmio32,0xff550000 console=ttyFIQ0 init=/ini0
[    0.000000] PID hash table entries: 4096 (order: 3, 32768 bytes)
[    0.000000] Dentry cache hash table entries: 262144 (order: 9, 2097152 bytes)
[    0.000000] Inode-cache hash table entries: 131072 (order: 8, 1048576 bytes)
[    0.000000] software IO TLB [mem 0x7fe57000-0x7fe97000] (0MB) mapped at [ffffffc07fe57000-ffffffc07fe96fff]
[    0.000000] Memory: 2040696K/2095168K available (2302K kernel code, 252K rwdata, 872K rodata, 256K init, 364K bss, 54472K reserved, 0K cma-reserved)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     modules : 0xffffff8000000000 - 0xffffff8008000000   (   128 MB)
[    0.000000]     vmalloc : 0xffffff8008000000 - 0xffffffbdbfff0000   (   246 GB)
[    0.000000]       .init : 0xffffff80083a0000 - 0xffffff80083e0000   (   256 KB)
[    0.000000]       .text : 0xffffff8008080000 - 0xffffff80082c0000   (  2304 KB)
[    0.000000]     .rodata : 0xffffff80082c0000 - 0xffffff80083a0000   (   896 KB)
[    0.000000]       .data : 0xffffff80083e0000 - 0xffffff800841f008   (   253 KB)
[    0.000000]     fixed   : 0xffffffbffe7fd000 - 0xffffffbffec00000   (  4108 KB)
[    0.000000]     PCI I/O : 0xffffffbffee00000 - 0xffffffbfffe00000   (    16 MB)
[    0.000000]     memory  : 0xffffffc000000000 - 0xffffffc080000000   (  2048 MB)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=2, Nodes=1
[    0.000000] Preemptible hierarchical RCU implementation.
[    0.000000]  Build-time adjustment of leaf fanout to 64.
[    0.000000] NR_IRQS:64 nr_irqs:64 0
[    0.000000] GIC: Using split EOI/Deactivate mode
[    0.000000] CPU0: found redistributor 0 region 0:0x00000000ff140000
[    0.000000] rockchip_clk_register_muxgrf: regmap not available
[    0.000000] rockchip_clk_register_branches: failed to register clock clk_32k_ioe: -524
[    0.000000] Architected cp15 timer(s) running at 24.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x588fe9dc0, max_idle_ns: 440795202592 ns
[    0.000004] sched_clock: 56 bits at 24MHz, resolution 41ns, wraps every 4398046511097ns
[    0.001113] Calibrating delay loop (skipped), value calculated using timer frequency.. 48.00 BogoMIPS (lpj=24000)
[    0.002058] pid_max: default: 32768 minimum: 301
[    0.002575] Mount-cache hash table entries: 4096 (order: 3, 32768 bytes)
[    0.003191] Mountpoint-cache hash table entries: 4096 (order: 3, 32768 bytes)
[    0.004432] sched-energy: CPU device node has no sched-energy-costs
[    0.005045] Invalid sched_group_energy for CPU0
[    0.005461] CPU0: update cpu_capacity 1024
[    0.005868] ASID allocator initialised with 65536 entries
[    0.015586] Detected VIPT I-cache on CPU1
[    0.015609] CPU1: found redistributor 1 region 0:0x00000000ff160000
[    0.015633] Invalid sched_group_energy for CPU1
[    0.015637] CPU1: update cpu_capacity 1024
[    0.015639] CPU1: Booted secondary processor [410fd042]
[    0.015708] Brought up 2 CPUs
[    0.018202] SMP: Total of 2 processors activated.
[    0.018635] CPU features: detected feature: GIC system register CPU interface
[    0.019294] CPU features: detected feature: 32-bit EL0 Support
[    0.019830] CPU: All CPU(s) started at EL2
[    0.020273] Invalid sched_group_energy for CPU1
[    0.020701] Invalid sched_group_energy for Cluster1
[    0.021149] Invalid sched_group_energy for CPU0
[    0.021564] Invalid sched_group_energy for Cluster0
[    0.022288] devtmpfs: initialized
[    0.028104] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275000 ns
[    0.029028] futex hash table entries: 512 (order: 3, 32768 bytes)
[    0.029845] pinctrl core: initialized pinctrl subsystem
[    0.034518] cpuidle: using governor menu
[    0.034907] Registered FIQ tty driver
[    0.035339] vdso: 2 pages (1 code @ ffffff80082c6000, 1 data @ ffffff80083e4000)
[    0.036048] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    0.036763] DMA: preallocated 256 KiB pool for atomic allocations
[    0.045229] fiq debugger fiq mode enabled
[[    0.045901] console [ttyFIQ0] enabled
    0.045901] console [ttyFIQ0] enabled
[    0.046590] bootconsole [uart0] disabled
[    0.046590] bootconsole [uart0] disabled
[    0.047140] Registered fiq debugger ttyFIQ0
[    0.054216] usbcore: registered new interface driver usbfs
[    0.054284] usbcore: registered new interface driver hub
[    0.054357] usbcore: registered new device driver usb
[    0.055417] rockchip-cpuinfo cpuinfo: Serial         : 0000000000000000
[    0.055690] clocksource: Switched to clocksource arch_sys_counter
[    0.057236] Unpacking initramfs...
[    0.971455] Freeing initrd memory: 13784K
[    0.971753] hw perfevents: enabled with armv8_cortex_a53 PMU driver, 7 counters available
[    0.976820] io scheduler noop registered (default)
[    0.977671] phy phy-fe010000.syscon:usb2-phy@100.1: Failed to get VBUS supply regulator
[    0.980570] dma-pl330 ff4e0000.dmac: Loaded driver for PL330 DMAC-241330
[    0.980602] dma-pl330 ff4e0000.dmac:         DBUFF-128x8bytes Num_Chans-8 Num_Peri-32 Num_Events-16
[    0.980928] rockchip-pvtm fe000000.syscon:npu-pvtm: failed to get rst 0 npu
[    0.981035] rockchip-pvtm fe020000.syscon:pmu-pvtm: failed to get rst 0 pmu
[    0.981131] rockchip-pvtm fe050000.syscon:pvtm: failed to get rst 0 core
[    0.981635] Serial: 8250/16550 driver, 5 ports, IRQ sharing disabled
[    0.982267] Unable to detect cache hierarchy for CPU 0
[    0.987531] rockchip-pinctrl pinctrl: unable to find group for node vsel-gpio
[    0.988710] fan53555-regulator 1-001c: FAN53555 Option[12] Rev[15] Detected!
[    0.988776] fan53555-reg: supplied by vcc5v0_sys
[    0.989293] vdd_npu: Bringing 1025000uV into 800000-800000uV
[    0.994423] rk3x-i2c ff500000.i2c: Initialized RK3xxx I2C bus at ffffff80084e6000
[    0.995321] rockchip-thermal ff3a0000.tsadc: failed to find thermal gpio state
[    0.995343] rockchip-thermal ff3a0000.tsadc: failed to find thermal otpout state
[    0.995534] rockchip-thermal ff3a0000.tsadc: tsadc is probed successfully!
[    0.995957] cpu cpu0: leakage=0
[    0.996083] cpu cpu0: Failed to get pvtm
[    0.996774] cpu cpu0: avs=0
[    0.997387] usbcore: registered new interface driver usbhid
[    0.997410] usbhid: USB HID core driver
[    0.998810] rockchip-pinctrl pinctrl: unable to find group for node pwr-key
[    0.999145] input: gpio-keys as /devices/platform/gpio-keys/input/input0
[    1.011597] Freeing unused kernel memory: 256K
Starting logging: OK
Welcome to Rockchip Linux SDK
Populating /dev using udev: [    1.054538] udevd[53]: error getting socket: Function not implemented
[    1.054584] udevd[53]: error initializing udev control socket
error getting socket: Function not implemented
done
Initializing random number generator... [    1.094790] random: dd: uninitialized urandom read (512 bytes read, 1 bits of entropy available)
done.
Starting network: ip: socket: Function not implemented
ip: socket: Function not implemented
FAIL
start power manager service...insmod NPU modules: [PowerManager] find_powerkey_devpath dev_path:/dev/input/event0 
[PowerManager]: pm_init
[PowerManager] open BACKLIGHT_BRIGHTNESS fail
[PowerManager] pm_power_init mBacklight_level=-1 
[PowerManager] open PM_KERNEL_THERMAL fail
[PowerManager] epoll_create failed(Function not implemented)
[PowerManager] key_event_handler enter! 
[    1.135100] galcore: loading out-of-tree module taints kernel.
[    1.138319] galcore: npu init.
[    1.139615] galcore: start npu probe.
[    1.140948] npu: platform_get_irq irq = 12
[    1.140967] npu: platform_get_resource registerMemBase = ffbc0000, registerMemSize = 1000
[    1.140976] Galcore version 6.3.3.203718
[    1.140982] Galcore options:
[    1.140987]   irqLine           = 12
[    1.140993]   registerMemBase   = 0xFFBC0000
[    1.141002]   registerMemSize   = 0x00001000
[    1.141008]   contiguousSize    = 0x00400000
[    1.141013]   contiguousBase    = 0x00000000
[    1.141018]   externalSize      = 0x00000000
[    1.141024]   externalBase      = 0x00000000
[    1.141029]   bankSize          = 0x00000000
[    1.141034]   fastClear         = -1
[    1.141039]   compression       = 15
[    1.141044]   powerManagement   = 1
[    1.141049]   baseAddress       = 0x00000000
[    1.141055]   physSize          = 0x00000000
[    1.141060]   recovery          = 1
[    1.141065]   stuckDump         = 0
[    1.141070]   gpuProfiler       = 1
[    1.141075]   userClusterMask   = 0x0
[    1.141080]   smallBatch        = 1
[    1.141084]   irqs              = 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
[    1.141108]   registerBases     = 0xFFBC0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
[    1.141133]   registerSizes     = 0x00001000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
[    1.141158]   chipIDs           = 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 
[    1.141184]   core 0 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFEC10000, 0xFFFFFFFFFFFFFFFF, 
[    1.141196]   core 1 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141209]   core 2 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141222]   core 3 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141234]   core 4 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141247]   core 5 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141259]   core 6 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141271]   core 7 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141283]   core 8 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141296]   core 9 sRAMBases = 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, 
[    1.141307] Build options:
[    1.141313]   gcdGPU_TIMEOUT    = 20000
[    1.141318]   gcdGPU_2D_TIMEOUT = 4000
[    1.141323]   gcdINTERRUPT_STATISTIC = 1
[    1.143487] Galcore ContiguousBase=0x78c00000 ContiguousSize=0x400000
[    1.147773] Galcore Info: MMU mapped core 0 SRAM[1] base=0xfec10000 size=0x200000
[    1.147813] Galcore Info: MMU mapped core 0 SRAM[1] hardware address=0xfec10000 size=0x200000
[    1.154627] galcore ffbc0000.npu: Init npu devfreq
[    1.154750] galcore ffbc0000.npu: leakage=0
[    1.167448] galcore ffbc0000.npu: temp=43750, pvtm=85022 (85490 + -468)
[    1.167492] galcore ffbc0000.npu: pvtm-volt-sel=2
[    1.167921] galcore ffbc0000.npu: avs=0
OK
/usr/bin/start_usb.sh: line 80: can't create /sys/kernel/config/usb_gadget/rockchip/UDC: nonexistent directory
umount: can't unmount /sys/kernel/config: Invalid argument
[    1.215021] file system registered
[root@buildroot:/]# [    2.207107] phy phy-fe010000.syscon:usb2-phy@100.1: charger = USB_SDP_CHARGER
start rknn server, version:0.9.7 (d65a37f build: 2019-05-31 14:23:01)
I NPUTransfer: Starting NPU Transfer Server, Transfer version 1.9.2 (126ddbf@2019-05-20T09:15:08)
[    2.253016] read descriptors
[    2.253053] read strings
[    2.373247] android_work: did not send uevent (0 0           (null))
[    2.488316] android_work: sent uevent USB_STATE=CONNECTED
[    2.496246] configfs-gadget gadget: super-speed config #1: b
[    2.496363] android_work: sent uevent USB_STATE=CONFIGURED
```