# ARM TrustZone

ARM `TrustZone` 기술은 모든 `Cortex-A` 클래스 프로세서에 기본적으로 적용되며 **ARM 아키텍처 보안 확장 목적으로 도입되었습니다.**

이러한 확장 기능은 공급 업체, 플랫폼 및 응용 프로그램에서 **일관된 프로그래머 모델을 제공하는 동시에 실제 하드웨어 지원으로 보안 환경을 제공**합니다.

ARM `TrustZone` 기술은 보안 지불, 디지털 권한 관리 (DRM), 엔터프라이즈 서비스 및 웹 기반 서비스를 포함한 고성능 컴퓨팅 플랫폼의 **광범위한 응용 프로그램을위한 시스템 차원의 보안 접근 방식**입니다.

`TrustZone` 기술은 Cortex-A 프로세서와 긴밀하게 통합되며 AMBA AXI 버스 및 특정 TrustZone 시스템 IP 블록을 통해 확장되므로 **ARM TrustZone 기술은 하드웨어 수준에서 제공되는 보안 메커니즘**입니다.

**이 시스템 접근 방식은 소프트웨어 공격으로부터 보안 메모리, 암호화 블록, 키보드 및 화면과 같은 주변 장치를 보호합니다.**

ARM Trusted Firmware 및 OP-TEE OS는 현재 ARM TrustZone 기술의 ARM 오픈 소스 프로젝트에서 사용되며, 함께 사용하거나 단독으로 사용할 수있는 ARM 칩의 기본 펌웨어 오픈 소스 프로젝트입니다.



## 1. 시스템 아키텍처

시스템 아키텍처 관점에서 다음은 ARM TrustZone 기술이 활성화 된 64 비트 플랫폼 시스템 아키텍처 다이어그램입니다.



![trustzone_0](./trustzone_0.png)



전체 시스템은 왼쪽의 비보안(Non-secure)과 오른쪽의 보안(Secure) 총  두 가지 세계로 나뉘어져 있습니다.

- 보안 세계는 두 세계의 모든 자원에 접근 할 수 있습니다.
- 비보안 세계는 비보안 세계의 리소스에만 액세스 할 수 있습니다.
  - 비보안 세계가 보안 세계의 자원에 액세스하면 시스템 하드웨어 버스 오류와 같은 예외가 발생하여 자원을 확보 할 수 없습니다.

**이 두 세계 간의 상호 작용을 위해서는 ARM Trusted Firmware를 브리지로 사용해야합니다.**

그래서 CPU가 비보안 세계에있을 때 보안 세계에 접근하려면 아래의 순서를 따라야 합니다.  

1. 먼저 ARM Trusted Firmware (ARM의 SMC 하드웨어 명령어)를 입력해야합니다.
2. 그런 다음 ARM Trusted Firmware의 보안 모니터 코드는 CPU를 비보안 ID에서 보안 ID로 전환합니다.
3. 위의 과정을 거치면 접근 주체의 ID가 보안상태로 전환되어 보안세계의 자원에 접근할 수 있습니다.

이 반대의 경우도 마찬가지입니다.  
이는 하드웨어 수준에서 보안 ID에서 비보안 ID로 완전히 전환됩니다.  



## 2. CPU 권한 수준

CPU 관점에서 다음은 ARM TrustZone이 활성화 된 표준 CPU 권한 모드 수준 아키텍처 다이어그램입니다.

64 비트 CPU 인 경우 권한 수준은 EL0, EL1, EL2, EL3으로 나뉘며, 이는 CPU가있는 세계에 따라 보안 EL0, 보안 EL1 또는 비보안 EL0, 비보안 EL1로 나뉩니다.

32 비트 CPU 인 경우 권한 수준은 Mon, Hyp, SVC, ABT, IRQ, FIQ, UND, SYS, USER 모드로 구분되며, SVC, ABT, IRQ, FIQ, UND, SYS, USER는 또한 64 비트와 마찬가지로 보안 모드와 비보안 모드 차이가 있습니다.

![trustzone_0](./trustzone_1.png)



![trustzone_0](./trustzone_2.png)



# Rockchip에서의 Trust

Rockchip Trust는 ARM의 신뢰할 수있는 펌웨어 + OP-TEE OS의 기능 모음으로 이해 될 수 있으며, 보안 세계에서 필요한 기능과 Secure Monitor (2 개의 세계 전환을위한 핵심 코드)의 기능을 구현합니다.

## 구현 메커니즘

현재 ARM Trusted Firmware + OP-TEE OS의 조합은 Rockchip 플랫폼의 64 비트 SoC 플랫폼에서 사용됩니다. OP-TEE OS는 32 비트 SoC 플랫폼에서 사용됩니다.

## 부팅 과정

ARM Trusted Firmware 아키텍처는 전체 시스템을 `EL0`, `EL1`, `EL2` 및 `EL3`의 4 가지 보안 수준으로 나눕니다.

전체 보안 부팅의 프로세스 단계는  
`BL1`, `BL2`, `BL31`, `BL32`, `BL33`으로 정의되며  
`BL1`, `BL2` 및 `BL31`의 기능은  
ARM Trusted Firmware 자체의 소스 코드에 제공됩니다.  

**Rockchip 플랫폼은 BL31의 기능 만 사용합니다.**

BL1 및 BL2에는 자체 구현 방법이 있습니다.

따라서 Rockchip 플랫폼에서는 일반적으로 "기본"ARM Trusted Firmware가 BL31을 참조하는 반면 BL32는 OP-TEE OS를 사용합니다.



위의 상태 정의가 Rockchip 플랫폼 펌웨어에 매핑 된 경우 아래와 같이 구성됩니다.

- Maskrom (BL1)
- Loader (BL2)
- Trust (BL31 : ARM Trusted Firmware + BL32 : OP-TEE OS)
- U-Boot (BL33).

>  안드로이드 시스템 펌웨어 부팅 순서 :
> `Maskrom-> loader-> trust-> u-boot-> kernel-> Android`



## Rockchip에서의 Trust 트러블슈팅 

<b style="color:red">현재 배포된 된 펌웨어에는 trust 바이너리 파일 만 제공되며 소스 코드는 제공되지 않습니다.</b>

**현재는 trust에 대한 디버깅 방법이 거의 없습니다.**

일반적으로 분석을 위해 특수 jtag 도구를 사용해야합니다.

trust에 문제가있는 경우 고객은 일반적으로 문제해결이 어렵지만 아래와 같은 방법으로 문제를 디버깅하고 해결할 수 있습니다.

- **문제가 발생했을 때 장면을 보호하고 trust를 담당하는 관리자에게 피드백을 제공하기에 충분한 정보를 수집하십시오.**
- **따라서 사용자는 일반적으로 트러스트로부터 출력되는 정보(메시지), 트러스트에 해당하는 버전 번호 및 트러스트의 PANIC 정보를 알고 있어야합니다.**