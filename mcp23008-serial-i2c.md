# 시리얼 인터페이스 (Serial interface)

이 블록에서는 I2C(MCP23008) 또는 SPI (MCP23S08)의 인터페이스 프로토콜 기능들을 다룹니다.

MCP23X08에는 직렬 인터페이스 블록을 통해 처리 할 수있는 11 개의 레지스터가 있습니다.

| Address | Access to:         |
| ------- | ------------------ |
| 00h     | IODIR              |
| 01h     | IPOL               |
| 02h     | GPINTEN            |
| 03h     | DEFVAL             |
| 04h     | INTCON             |
| 05h     | IOCON              |
| 06h     | GPPU               |
| 07h     | INTF               |
| 08h     | INTCAP (Read-only) |
| 09h     | GPIO               |
| 0Ah     | OLAT               |

> 이 문서에서는 MCP 23008의 I2C 인터페이스 프로토콜 기능만 다룸


## 순차 작동 비트 (Sequential operation bit)

IOCON 레지스터의 순차 연산 (SEQOP) 비트는 주소 포인터의 작동을 제어합니다.

> (IOCON.SEQOP = 1 or 0)

![image](reg_iocon.png)

주소 포인터는 각 데이터 전송 후 주소 포인터가 자동으로 증가하도록 활성화 (기본값)하거나 비활성화 할 수 있습니다.  

- 순차 모드 (`IOCON.SEQOP = 0`)에서 작동하는 경우 주소 포인터는 각 바이트가 클럭 된 후 다음 주소로 자동 증가합니다.
- 바이트 모드 (`IOCON.SEQOP = 1`) 에서 작동하는 경우 MCP23008은 데이터 전송 중 각 바이트가 클럭 된 후 주소 카운터를 증가시키지 않습니다.
  - 이를 통해 추가 제어 바이트 없이 추가 클럭을 제공하여 동일한 주소를 지속적으로 읽거나 쓸 수 있습니다.
  - 데이터 변경을 위해 GPIO 레지스터를 폴링할 때 유용합니다.



## I2C 인터페이스

### 제어
- MCP23008는 R/W bit가 제어바이트에 포함된 7bit slave 주소지정방식을 지원하는  `I2C slave`장치입니다.
- slave 주소는 4개의 고정된 비트와 유저가 설정한 3개의 비트를 포함합니다.
- (A2, A1, A0의 입력에 따라 달라짐.)

![image](address_pinout.png)

![image](addressing0.png)

- 제어를 위한 slave 주소비트는 `010 0xxx`.
  - `0x20 ~ 0x27`까지 

- ACK는 




### I2C 쓰기 작동(Write operation)

- I2C 쓰기 작업에는 제어 바이트 및 레지스터 주소 시퀸스가 포함됩니다.

  ![image](write1.png)

- 이 시퀀스 다음에 마스터에서 8 비트 데이터를 내보내면 MCP23008에서 ACK(Acknowledge)를 반환합니다.

![image](addressing1.png)

![image](write2.png)

- 마스터에 의해 STOP 또는 RESTART 조건이 생성되면서 작업이 종료됩니다.

![image](write3.png)



- 모든 바이트 전송 후 MCP23008에 데이터가 기록됩니다.
- 데이터 전송 중에 STOP 또는 RESTART 조건이 생성되면 데이터가 MCP23008에 기록되지 않습니다.



### I2C 읽기 작동 (Read operation)

- I2C 읽기 작업에는 제어 바이트가 포함됩니다.

![image](read1.png)

- 이 시퀀스 다음에는 R/W 비트가 논리 1인 다른 제어 바이트가 이어집니다.
  - (R: 1, W=0)
  - (R/W=1, START 조건 및 ACK를 포함한 제어바이트)

![image](read2.png)

- 그런 다음 MCP23008은 주소 지정된 레지스터에 포함 된 데이터를 전송합니다.

![image](read3.png)

- 마스터가 STOP 또는 RESTART 조건을 생성하면 시퀀스가 종료됩니다.

![image](read4.png)



### I2C 순차적 읽기/쓰기

- 순차 작업(쓰기 또는 읽기)의 경우 데이터 전송 후 STOP 또는 RESTART 조건을 전송하는 대신 마스터는 주소 포인터가 가리키는 다음 바이트를 클럭합니다.
  - (순차 운전 제어에 관한 자세한 내용은 [순차 작동 비트 (Sequential operation bit)](## 순차 작동 비트 (Sequential operation bit)) 참조)

![image](sequence0.png)

- 마스터가 STOP 또는 RESTART 조건을 보내는 것으로 시퀀스가 종료됩니다.

![image](sequence1.png)

- MCP23008 주소 포인터는 마지막 레지스터 주소에 도달 한 후 주소 0으로 돌아감.

---
