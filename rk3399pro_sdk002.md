---
title: RK3399PRO SDK 가이드문서
date: 2019-04-17 10:22:30
categories: SDM2
excerpt: RK3399PRO의 SDK를 다루는 방법에 대한 문서 
tags:
  - RK3399PRO
  - Rockchip
  - SDK
---
# 특이사항
**1. RK3399PRO_LINUX_SDK_V0.02_20190321_190422.tar.gz 사용할것.**  
기존 SDK(v0.1)에 자질구레한 이슈가 있었음.  
마이크로텍에 새로 요청하여 `RK3399PRO_LINUX_SDK_V0.02_20190321_190422.tar.gz` 내려받아 사용함.  
**문서는 새로 내려받은 SDK를 기준으로 작성됨.**

**2. 기능관련 문서는 `$SDK/docs/`에 있음.**  
이 SDK는 Linux Buildroot 및 Debian 9 시스템(커널 v4.4)을 기반으로 하며, Linux 제품을 기반으로 하는 RK3399Pro EVB 개발에 적합합니다.  
이 SDK는 NPU Tensorflow/Caffe 모델, VPU 하드 디코딩, GPU 3D, Wayland 디스플레이, QT 및 기타 기능을 지원합니다.  
특정 함수 디버깅 및 인터페이스 설명은 `$SDK/docs/`의 문서를 참조하십시오.

**3. NPU 관련 리소스**  
NPU의 개발 정보 : `$SDK/external/rknpu/` 참조  
RKNN Demo : `$SDK/external/rknn_demo/` 참조  

# SDK 컴파일
## 패키지 설치
사용할 rootfs에 따라 설치할 패키지 종류가 다름.
debian 혹은 buildroot중 하나 선택.
- 2019.05.10 : buildroot 사용환경 확인. rknn 사용 불가
- 2019.06.10 : debian 사용환경 확인. rknn 정상 사용 가능

### 참고사항 
**Ubuntu 16.04 환경에서 진행할 경우**  
문서대로 패키지 설치하고 진행

**Ubuntu 17.04 이후의 시스템에서 진행할 경우**  
문서대로 패키지 설치하고 진행하되 아래의 사항에 준수할 것.
> <b style='color:red'>`gcc-4.8-multilib-arm-linux-gnueabihf` 설치하지 마십시오.</b>
```bash
sudo apt-get install lib32gcc-7-dev g++-7 libstdc++-7-dev
```
- RK3399Pro는 전원이 켜질 때마다 NPU 펌웨어를 로드합니다.  
- 기본 NPU 펌웨어는 모두 사전에 컴파일 되어 rootfs의 `/usr/share/npu_fw` 하위에 생성됩니다.  
- 펌웨어 설정 및 생성방법은 `$SDK/docs/Soc Platform related/RK3399PRO/RK3399PRO_NPU 전원 부팅.pdf` 파일을 참조하십시오.

### Buildroot
```bash
# 업데이트
sudo apt-get update

# 패키지 설치
sudo apt-get install repo git-core gitk git-gui \
gcc-arm-linux-gnueabihf u-boot-tools device-tree-compiler \
gcc-aarch64-linux-gnu mtools parted libudev-dev libusb-1.0-0-dev \
python-linaro-image-tools linaro-image-tools autoconf \
autotools-dev libsigsegv2 m4 intltool libdrm-dev curl sed make \
binutils build-essential gcc g++ bash patch gzip bzip2 perl tar cpio python \
unzip rsync file bc wget libncurses5 libqt4-dev libglib2.0-dev libgtk2.0-dev \
libglade2-dev cvs git mercurial rsync openssh-client subversion asciidoc \
w3m dblatex graphviz python-matplotlib libc6:i386 libssl-dev texinfo \
liblz4-tool genext2fs
```

### Debian
```bash
sudo apt-get install repo git-core gitk git-gui gcc-arm-linux-gnueabihf \
u-boot-tools device-tree-compiler gcc-aarch64-linux-gnu mtools parted \
libudev-dev libusb-1.0-0-dev python-linaro-image-tools linaro-image-tools \
gcc-4.8-multilib-arm-linux-gnueabihf gcc-arm-linux-gnueabihf libssl-dev \
gcc-aarch64-linux-gnu g+conf autotools-dev libsigsegv2 m4 intltool \
libdrm-dev curl sed make binutils build-essential gcc g++ bash patch gzip \
bzip2 perl tar cpio python unzip rsync file bc wget libncurses5 libqt4-dev \
libglib2.0-dev libgtk2.0-dev libglade2-dev cvs git mercurial rsync \
openssh-client subversion asciidoc w3m dblatex graphviz python-matplotlib \
libc6:i386 libssl-dev texinfo liblz4-tool genext2fs
```


## NPU 이미지 생성
### U-Boot
`$SDK/npu/u-boot` 디렉터리에서 `make.sh`를 실행하여 `rk3399pro_npu_loader_v1.01.100.bin`, `trust.img`, `uboot.img` 생성. 

```bash
cd $SDK/npu/u-boot
sudo ./make.sh rk3399pro-npu

>> tree -L 1
.
├── rk3399pro_npu_loader_v1.01.100.bin
├── ...
├── trust.img
└── uboot.img

```

### Kernel
> rk3399pro evb :  
```bash
cd $SDK/npu/kernel
sudo make ARCH=arm64 rk3399pro_npu_defconfig
sudo make ARCH=arm64 rk3399pro-npu-evb-v10.img -j12
```

### Boot.img 및 NPU 펌웨어 생성 단계
```bash
cd $SDK/npu
sudo ./build.sh ramboot
sudo ./mkfirmware.sh
# rk3399pro_npu 선택할 것.
```
## RK3399Pro 이미지 생성
### U-Boot
`$SDK/u-boot`에서 `make.sh` 실행하여 `rk3399pro_loader_v1.15.115.bin`, `trust.img`, `uboot.img` 생성.
```bash
cd $SDK/u-boot
sudo ./make.sh rk3399pro

>> tree -L 1
.
├── rk3399pro_loader_v1.15.115.bin
├── ...
├── trust.img
└── uboot.img

```

### Kernel
```bash
cd $SDK/kernel
sudo make ARCH=arm64 rockchip_linux_defconfig

# rk3399pro evb v10 일경우
sudo make ARCH=arm64 rk3399pro-evb-v10-linux.img -j8

# rk3399pro evb v11 일경우
sudo make ARCH=arm64 rk3399pro-evb-v11-linux.img -j8
```
> 컴파일 후 kernel 디렉터리에 `boot.img` 파일이 생성되는데 이는 kernel과 dtb들이 함께 패키징 된 파일입니다.

### Recovery
```bash
cd $SDK/
sudo ./build.sh recovery
```
> 컴파일 이후 Buildroot 디렉터리의 `output/rockchip_rk3399pro_recovery/images`에 `recovery.img`파일이 생성됩니다.  

### Rootfs
Buildroot 및 Debian 중 하나 선택  
#### Buildroot rootfs  
```bash
cd $SDK/
./build.sh rootfs
```
> 컴파일이 끝나면 `Buildroot` 디렉토리의 `rockchip_rk3399pro/images/`에 `rootfs.ext4`가 생성됩니다.

#### Debian rootfs  
```bash
cd $SDK/debian/
```

##### linaro의 `ubuntu-build-service`을 이용한 Debian 기반 시스템 빌드
```bash
cd $SDK/debian/
sudo apt-get install binfmt-support qemu-user-static live-build
sudo dpkg -i ubuntu-build-service/packages/*
sudo apt-get install -f
```
32bit Debian:
```bash
cd $SDK/debian/
RELEASE=stretch TARGET=desktop ARCH=armhf ./mk-base-debian.sh
```
64bit Debian:
```bash
cd $SDK/debian/
RELEASE=stretch TARGET=desktop ARCH=arm64 ./mk-base-debian.sh
```
> 명령 실행시 `debian/` 내부에 linaro-stretch-alip-xxxxx-1.tar.gz 파일 생성됩니다.(xxxxx는 타임스탬프)

##### rk-debian rootfs 빌드
32bit Debian:
```bash
# no debug
cd $SDK/debian/
RELEASE=stretch ARCH=armhf ./mk-rootfs.sh
# with debug
cd $SDK/debian/
VERSION=debug ARCH=armhf ./mk-rootfs-stretch.sh
```
64bit Debian:
```bash
# no debug
cd $SDK/debian/
RELEASE=stretch ARCH=arm64 ./mk-rootfs.sh
# with debug
cd $SDK/debian/
VERSION=debug ARCH=arm64 ./mk-rootfs-stretch-arm64.sh
```

##### ext4 이미지 생성
```bash
cd $SDK/debian/
./mk-image.sh
```
명령 실행시 `linaro-rootfs.img` 파일 생성됩니다.

                                               
### 이미지 패키징
#### 준비하기
RK3399PRO EVB의 보드 설정은 아래와 같습니다.
```bash
# Target arch
export RK_ARCH=arm64
# Uboot defconfig
export RK_UBOOT_DEFCONFIG=rk3399pro
# Kernel defconfig
export RK_KERNEL_DEFCONFIG=rockchip_linux_defconfig
# Kernel dts
export RK_KERNEL_DTS=rk3399pro-evb-v11-linux
# boot image type
export RK_BOOT_IMG=boot.img
# kernel image path
export RK_KERNEL_IMG=kernel/arch/arm64/boot/Image
# parameter for GPT table
export RK_PARAMETER=parameter-buildroot.txt
# Buildroot config
export RK_CFG_BUILDROOT=rockchip_rk3399pro
# Recovery config
export RK_CFG_RECOVERY=rockchip_rk3399pro_recovery
```
설정 확인 및 변경은 SDK 디렉터리의 `device/rockchip/rk3399pro/`에서 확인 가능합니다.

#### 패키징
위에서 이미지 빌드 및 보드 설정을 마친 이후 SDK디렉터리의 루트로 돌아와 다음의 명령을 실행하십시오.  
```bash
# Debian 이미지 패키징 준비
sudo ./build.sh BoardConfig_debian.mk

# Buildroot 이미지 패키징 준비
sudo ./build.sh BoardConfig.mk

# 패키징
sudo ./mkfirmware.sh
```

# 이미지 플래싱
## Linux 환경에서 작업
Linux에서 사용가능한 플래싱도구는 `$SDK/tools/linux/Linux_Upgrade_Tool/Linux_Upgrade_Tool`디렉터리에 있습니다.  
`/usr/bin`에 복사하여 원활하게 사용할 수 있습니다.
```bash
sudo cp $SDK/tools/linux/Linux_Upgrade_Tool/Linux_Upgrade_Tool/upgrade_tool /usr/bin

sudo upgrade_tool
```
보드가 `MASKROM / loader rockusb`상태인지 확인하고  
SDK의 `rockdev`디렉터리로 진입하십시오.
```bash
cd $SDK/rockdev
```

이후 플래싱에 사용될 명령은 아래와 같습니다.
```bash
# 보드 연결 상태 확인
sudo upgrade_tool ld

sudo upgrade_tool ul MiniLoaderAll.bin
sudo upgrade_tool di -p parameter.txt
sudo upgrade_tool di -u uboot.img
sudo upgrade_tool di -t trust.img
sudo upgrade_tool di -misc misc.img
sudo upgrade_tool di -b boot.img
sudo upgrade_tool di -recovery recovery.img
sudo upgrade_tool di -oem oem.img
sudo upgrade_tool di -rootfs rootfs.img
sudo upgrade_tool di -userdata userdata.img

# 보드 재부팅
sudo ./upgrade_tool rd
```

# Tip
## 파티션 설정
플래싱 이후 보드를 부팅하여 직접 파티션을 변경하는데 어려움이 있습니다. (변경 이후 부팅안되는 이슈)

그래서  
parameter.txt 에서 각 파티션 크기를 변경하는 방법을 사용합니다.

**예시**  
Debian의 rootfs 공간을 늘린 예시.  (Over 10GB)  

|Number|Start (sector)|End (sector)|Size|Code|Name|
|-|-|-|-|-|-|
|1|0x4000|0x6000|0x2000|0700|uboot|
|2|0x6000|0x8000|0x2000|0700|trust|
|3|0x8000|0xa000|0x2000|0700|misc|
|4|0xa000|0x1a000|0x10000|0700|boot|
|5|0x1a000|0x4a000|0x30000|0700|recovery|
|6|0x4a000|0x5a000|0x10000|0700|backup|
|7|0x5a000|0x7a000|0x20000|0700|oem|
|8|0x7a000|0x137a000|0x1300000|0700|rootfs|
|9|0x137a000|END|(Grow to the end)|0700|userdata|
