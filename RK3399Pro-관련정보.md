# 관련정보

## 연결
- RK3399Pro의 NPU는 USB로 연결되는 장치
- USB2.0을 통해 펌웨어(NPU 구동에 필요한 이미지) 전달받음.
- USB3.0을 통해 연산정보 공유

![npu_conn](./npu_conn.png)

- EVB는 부팅 이후 마지막에 NPU를 USB장치로 연결 시도
  - `/etc/init.d/rockchip.sh` 실행
    - `/usr/bin/npu-image.sh`: NPU에 펌웨어 플래싱
    - `/usr/bin/npu_transfer_proxy &`: NPU통신 시작

```bash
function update_npu_fw() {
    /usr/bin/npu-image.sh
    sleep 1
    /usr/bin/npu_transfer_proxy&
}

COMPATIBLE=$(cat /proc/device-tree/compatible)
if [[ $COMPATIBLE =~ "rk3288" ]];
then
    CHIPNAME="rk3399pro"
    update_npu_fw
elif [[ $COMPATIBLE =~ "rk3399" ]]; then
    CHIPNAME="rk3399"
else
    CHIPNAME="rk3036"
fi
COMPATIBLE=${COMPATIBLE#rockchip,}
BOARDNAME=${COMPATIBLE%%rockchip,*}
```

**EVB 부팅 순서의 마지막단계에 위 과정을 거치면서 아래의 순서가 추가로 작동한다는 결론.**

`NPU전원 인가 > USB장치 인식 > NPU 펌웨어 플래싱 >  NPU통신 시작`

***

## 전원

`/usr/bin/npu_powerctrl`을 실행하여 NPU의 전원을 제어  
실행시 다음과 같은 옵션이 필요  

```bash
rk3399pro:/# npu_powerctrl
Usage : npu_powerctrl [-s] [-r] [-o] [-i] [=d]
-s : NPU를 'SLEEP' 모드로 전환
-r : NPU를 'WAKEUP' 모드로 전환
-o : NPU의 전원을 인가 # '전원리셋을 의미'
-i : NPU의 'GPIO'를 초기화
-d : NPU의 전원 종료
```

### 사용예시

```bash
root@linaro-alip:/usr/share/npu_fw# npu_powerctrl -o
set clk_en  0 to 1
powerup
[ 1551.707276] usb 3-1: new high-speed USB device number 9 using xhci-hcd
[ 1551.822630] usb 3-1: New USB device found, idVendor=2207, idProduct=180a
[ 1551.823343] usb 3-1: New USB device strings: Mfr=0, Product=0, SerialNumber=0
```

- **전원을 켜면 RK3399Pro가 NPU를 USB장치로 인식함.**

- **NPU의 DeviceID는 `1808`혹은 `180a`로 구분**
  - 아래와 같이 확인, 안잡히면 메시지 안나옴.

```bash
root@linaro-alip:~# dmesg | grep 180a                                     
[    3.426468] usb 3-1: New USB device found, idVendor=2207, idProduct=180a
[    4.850990] usb 3-1: New USB device found, idVendor=2207, idProduct=180a
root@linaro-alip:~# dmesg | grep 1808
[   10.305067] usb 4-1: New USB device found, idVendor=2207, idProduct=1808
root@linaro-alip:~# 
```

***

## 펌웨어

### 복사된 위치

- 펌웨어는 `/usr/share/npu_fw`에 위치
  - `MiniLoaderAll.bin`
  - `boot.img`
  - `trust.img`
  - `uboot.img`

### 플래싱

**`/usr/bin/npu_upgrade`로 플래싱 가능 (NPU 2GB 램디스크 사용)**
아래의 예시와 같이 스크립트 실행하여 NPU가 USB장치로 잡혔는지 확인할 수 있음.

```bash
root@linaro-alip:~# cd /usr/share/npu_fw
root@linaro-alip:/usr/share/npu_fw# npu_upgrade MiniLoaderAll.bin uboot.img trust.img boot.img
```

#### 성공

```bash
[  135.092105] wl_run_escan: LEGACY_SCAN sync ID: 4, bssidx: 0
init gpio: 4
Error writing to /sys/class/gpio/export value=4: 
gpio init
set clk_en  1 to 0
set clk_en  0 to 1
[  135.204958] usb 1-1: USB disconnect, device number 3
powerup
start to wait device...
[  135.439353] usb 3-1: new high-speed USB device number 7 using xhci-hcd
[  135.554602] usb 3-1: New USB device found, idVendor=2207, idProduct=180a
[  135.555273] usb 3-1: New USB device strings: Mfr=0, Product=0, SerialNumber=0
device is ready
start to download loader...
[  136.281741] usb 3-1: reset high-speed USB device number 7 using xhci-hcd
[  136.399074] usb 3-1: device descriptor read/64, error -71
[  136.614791] usb 3-1: device firmware changed
[  136.618020] usb 3-1: USB disconnect, device number 7
[  136.724310] usb 3-1: new high-speed USB device number 8 using xhci-hcd
[  136.840560] usb 3-1: New USB device found, idVendor=2207, idProduct=180a
[  136.841190] usb 3-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[  136.842006] usb 3-1: Product: USB-MSC
[  136.842383] usb 3-1: Manufacturer: RockChip 
[  136.842772] usb 3-1: SerialNumber: rockchip
download loader ok                
start to wait loader...
loader is ready
start to write uboot...
write uboot ok
start to write trust...
write trust ok
start to write boot...            
write boot ok            
start to run system...                      
run system ok
linaro@linaro-alip:/usr/share/npu_fw$ [  140.411384] usb 3-1: USB disconnect, device number 8               
[  141.902234] usb 1-1: new SuperSpeed USB device number 4 using xhci-hcd 
[  141.916839] usb 1-1: New USB device found, idVendor=2207, idProduct=1808                                 
[  141.917505] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3                            
[  141.918417] usb 1-1: Product: rk3xxx                                   
[  141.918867] usb 1-1: Manufacturer: rockchip
[  141.919264] usb 1-1: SerialNumber: 0123456789ABCDEF  
```
#### 실패

```bash
init gpio: 4
Error writing to /sys/class/gpio/export value=4: 
gpio init
set clk_en  1 to 0
set clk_en  0 to 1
powerup
start to wait device...
1
2
3
4
5
set clk_en  1 to 0
set clk_en  0 to 1
powerup
reset npu to retry!!!
6
7
8
9
10
failed to wait device!
```

***

## 통신

RK3399Pro와 NPU가 통신하기 위해 무언가 처리를 하는 것으로 보임.   
<u>(이 부분은 공개되어있지 않아 해보고 느낀대로 적음)</u>  

- `/usr/bin/npu_transfer_proxy`를 실행하는 동안 NPU와 모델 연산결과를 주고받음.
  - 실행 안하면 딥러닝 모델 연산 불가
  - `&` 붙여서 백그라운드로 실행함
  - `galcore.ko`에 의존함.

> NPU와 통신을 하려면 Rockchip이 만든 RKNN-Toolkit에 의존해야하는데
>
> 초기버전 (1.0 이전)에서 `npu_transfer_proxy` 실행시
>
> 잘못 만들어진 `galcore.ko` 때문에 테스트가 불가능했지만 지금은 해결된 상태.



실행시 아래와 같은 메시지 확인 가능.

```bash
root@linaro-alip:~# /usr/bin/npu_transfer_proxy &
I NPUTransfer: Starting NPU Transfer Proxy, Transfer version 1.9.2 (126ddbf@2019-05-20T09:15:08), devid = 0123456789ABCDEF, pid = 1193:674
```

**이 부분은 NPU 연결상태와 관계없이 온전히 실행되어야함.**

***



## 부팅 메시지

RK3399Pro의 부트메시지는 첨부하기에 너무 길어서 아래의 링크로 대체

- 아래의 링크로 대체
  - [RK3399Pro](https://gitlab.com/aciddust/opensource/blob/master/RK3399Pro-Boot-Message.md)
  - [RK3399Pro-NPU](https://gitlab.com/aciddust/opensource/blob/master/RK3399Pro-NPU-Boot-Message.md)



## 디버깅 정보
### NPU Power IC
- 출력전압 0
  - 2019년 10월 1일 퇴근 전 확인
- Power Enable 신호 안찍어봄
- C412 찍어봄. (전압 나옴)
- Power Enable Pin
  - 1 : gpio4  
  - 2 : gpio10  
  - 3 : gpio11  
  - 4 : gpio54  
  - 5 : gpio55  
  - 6 : gpio56  

### 보드 초기화 정보
- 보드 부팅시 init.d의 rockchip.sh로 초기화를 진행하는 것으로 보임.
- rootfs를 PC에 mount하여 etc 내부 확인하였으나 power enable 관련 처리를 하는 스크립트 찾지 못함.  
 

### Serial Debugger
baudrate -> 1500000

- SDM2의 NPU시리얼 디버거 작동안함.
  - 전원이 안들어가서 작동안하는 것으로 판단.
